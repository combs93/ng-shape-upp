
// ! WARNING should be simplyfied to the simple "string" type !.
export interface ChartPriceItem {
  percentage: number;
  date: string;
  close: number;
  id: number;
}

export interface EarningsPerShare {
  [date: string]: {
    date: string;
    epsActual: number;
    epsEstimate: number;
    surprisePercent: number;
  };
}

export interface MainContent {
  general: {
    one_day: {
      profit: number;
      percentage: number;
    };
    year_to_date: {
      profit: number;
      percentage: number;
    };
    name: {
      code: string;
      name: string;
    };
    currency_symbol: string;
    component_of: [
      {
        name: string
      },
      {
        name: string
      }
    ],
    benchmark_index: string;
    price: number;
    market_capitalization: number;
    industry: string;
    sector: string;
    logo: string;
    exchange: string;
    currency: string;
    description: string;
    trading_time: {
      status: string;
      time: string;
    }
  };
  analyst_price: {
    current: number;
    upside_or_downside_potential: number;
    wall_street_target_price: number;
    week_high_52: number;
    week_low_52: number;
  };
  benchmark_index: {
    benchmark_index: string
  };
  chart: {
    price: {
      stock: ChartPriceItem[];
      index: ChartPriceItem[];
    },
    earnings_per_share: EarningsPerShare,
    vector: any // TODO типізацію
  };
  valuation: {
    stock: {
      current_pe_stock: number;
      forward_pe_stock: number;
    },
    index: {
      current_pe_index: any;
      forward_pe_index: number;
    }
  };
  dividend: {
    stock_dividend_yield: number;
    index_dividend_yield: number;
  };
  risk: {
    stok: {
      standard_deviation_stock: number;
      beta_stock: number;
    },
    index: {
      standard_deviation_index: {
        '1y_Volatility': number;
        '3y_Volatility': number;
      },
      beta_index: number
    }
  };
  growth: {
    code: string;
    exchange: string;
  };
  params: {
    revenue_quarter_yoy: number;
    earnings_quarter_yoy: number;
  };
  profitability: {
    operating_margin: number;
    return_on_equity: number;
  };
  type: string;
}
export interface EtfContent {
  analyst_price: {
    current: number;
    week_high_52: number;
    week_low_52: number;
  };
  benchmark_index: {
    benchmark_index: string;
  };
  chart: {
    price: {
      stock: ChartPriceItem[];
      index: ChartPriceItem[];
    },
    earnings_per_share: EarningsPerShare,
    vector: any // TODO типізацію
  };
  etf: {
    annual_holdings_turnover: number;
    average_mkt_cap_mil: string;
    company_name: string;
    company_url: any;
    date_ongoing_charge: any;
    dividend_paying_frequency: any;
    etf_url: any;
    exchange_id: number;
    exchange_symbol_id: number;
    id: number;
    inception_date: string;
    index_name: string;
    max_annual_mgmt_charge: any;
    net_expense_ratio: number;
    ongoing_charge: any;
    total_assets: number;
    yield: any
  };
  etf_allocation: etfAllocationItem[];
  etf_capitalisation: {
    big: any;
    etf_id: number;
    exchange_id: number;
    exchange_symbol_id: number;
    id: number;
    medium: any;
    mega: any;
    micro: any;
    small: any;
  };
  etf_fixed_income: etfFixedIncomeItem[];
  etf_holding: etfHoldingItem[];
  etf_morningstar: {
    category_benchmark: any;
    etf_id: number;
    exchange_id: number;
    exchange_symbol_id: number;
    id: number;
    ratio: any;
    sustainability_ratio: any;
  };
  etf_performance: {
    created_at: string;
    etf_id: number;
    exchange_id: number;
    exchange_symbol_id: number;
    exp_return_3y: number;
    id: number;
    returns_1y: number;
    returns_3y: number;
    returns_5y: number;
    returns_10y: number;
    returns_ytd: number;
    sharp_ratio_3y: number;
    updated_at: string;
    volatility_1y: number;
    volatility_3y: number;
  };
  etf_sector_weight: etfSectorWeightItem[];
  etf_valuations_growth: etfValuationGrowthItem[];
  etf_world_region: etfWorldRegionItem[];
  general: {
    one_day: {
      profit: number;
      percentage: number;
    };
    year_to_date: {
      profit: number;
      percentage: number;
    };
    name: {
      code: string;
      name: string;
    };
    currency_symbol: string;
    component_of: [
      {
        name: string
      },
      {
        name: string
      }
    ],
    benchmark_index: string;
    price: number;
    market_capitalization: number;
    industry: string;
    sector: string;
    logo: string;
    exchange: string;
    currency: string;
    description: string;
    trading_time: {
      status: string;
      time: string;
    }
  };
  params: {
    code: string;
    exchange: string;
  };
  technical: {
    beta: number;
    day_ma_50: string;
    day_ma_200: string;
    shares_short: any;
    shares_short_prior_month: any;
    short_percent: any;
    short_ratio: any;
    week_high_52: number;
    week_low_52: number;
  };
  type: string;
}

export interface etfAllocationItem {
  etf_id: number;
  exchange_id: number;
  exchange_symbol_id: number;
  id: number;
  long_percent: number;
  net_assets_percent: number;
  short_percent: number;
  type: string;
}
export interface etfFixedIncomeItem {
  etf_id: number;
  exchange_id: number;
  exchange_symbol_id: number;
  fund_percent: number;
  id: number;
  relative_to_category: number;
  type: string;
}
export interface etfHoldingItem {
  assets_percent: number;
  code: string;
  etf_id: number;
  exchange: any;
  exchange_id: number;
  exchange_symbol_id: number;
  id: number;
}
export interface etfSectorWeightItem {
  equity_percent: number;
  etf_id: number;
  exchange_id: number;
  exchange_symbol_id: number;
  id: number;
  relative_to_category: number;
  type: string;
}
export interface etfValuationGrowthItem {
  book_value_growth: any;
  cash_flow_growth: any;
  etf_id: number;
  exchange_id: number;
  exchange_symbol_id: number;
  historical_earnings_growth: any;
  id: number;
  long_term_projected_earnings_growth: any;
  price_book: number;
  price_cash_flow: number;
  price_prospective_earnings: number;
  price_sales: number;
  sales_growth: any;
  type: string;
}
export interface etfWorldRegionItem {
  equity_percent: number;
  etf_id: number;
  exchange_id: number;
  exchange_symbol_id: number;
  id: number;
  relative_to_category: number;
  type: string;
}

export interface SearchContentStock {
  code: string;
  exchange: string;
  name: string;
  type: string;
}

export interface SearchContent {
  stocks: SearchContentStock[];
  indices: [];
}
