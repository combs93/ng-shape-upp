export const verticalChartOptions = (
    verticalMinValue: number,
    verticalMaxValue: number,
    mainColor: any,
    chartColorBlue: any,
    verticalChartValues: any,
    verticalChartIndex: any
) => {
  return {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
        enabled: false
    },
    legend: {
        display: false
    },
    layout: {
        padding: {
            left: 0,
            right: 0,
            top: 30,
            bottom: 0
        }
    },
    scales: {
      yAxes: [{
          display: true,
          gridLines: {
              color: 'transparent',
              zeroLineColor: '#C9C9C9',
              drawTicks: false,
              drawBorder: false
          },
          ticks: {
              padding: 0,
              fontStyle: 'bold',
              min: verticalMinValue,
              max: verticalMaxValue,
              callback(value: any, index: any, labels: any) {
                  return ' ';
              }
          }
      }],
      xAxes: [{
          barThickness: 30,
          gridLines: {
              display: false,
              drawTicks: false,
              color: 'transparent',
              drawBorder: false
          },
          ticks: {
              padding: 50,
              fontColor: '#6F6E6E',
              fontSize: 11,
              callback(label: any, index: any, labels: any) {
                  if (/\s/.test(label)) {
                      return label.split(' ');
                  } else{
                      return label;
                  }
              }
          }
      }]
    },
    hover: {
      animationDuration: 1
    },
    animation: {
      duration: 0,
      onComplete: (chartProps: any) => {

        chartProps.chart.ctx.textAlign = 'center';
        chartProps.chart.ctx.textBaseline = 'top';

        verticalChartData(mainColor, chartColorBlue, verticalChartValues, verticalChartIndex).forEach((dataset, i) => {
            chartProps.chart.controller.getDatasetMeta(i).data.forEach((bar: any, index: any) => {

                if (dataset.data[index] > 0) {
                  chartProps.chart.ctx.fillText(dataset.data[index] + '%', bar._model.x, bar._model.y - 15);
                  chartProps.chart.ctx.fillText(dataset.label, bar._model.x, 240);
                } else {
                  chartProps.chart.ctx.fillText(dataset.data[index] + '%', bar._model.x, bar._model.y + 15);
                }
            });
        });
      }
    }
  };
};

export const verticalChartData = (mainColor: string, chartColorBlue: string, verticalChartValues: any[], verticalChartIndex: any[]) => {
  return [
    {
        label: 'AAPL',
        backgroundColor: mainColor,
        hoverBackgroundColor: mainColor,
        data: verticalChartValues
    },
    {
        label: 'Index',
        backgroundColor: chartColorBlue,
        hoverBackgroundColor: chartColorBlue,
        data: verticalChartIndex
    }
  ];
};
