export const horizontalChartOptions = (
    yAxesPadding: number,
    horizontalMinValue: number,
    horizontalMaxValue: number,
    chartProps: any,
    horizontalChartIndex: any[],
    horizontalColorsArr: any[]
  ) => {
  return {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
        enabled: false
    },
    legend: {
        display: false
    },
    layout: {
        padding: {
            left: 0,
            right: 30,
            top: 0,
            bottom: 0
        }
    },
    scales: {
        yAxes: [{
            display: true,
            barThickness: 30,
            gridLines: {
                color: 'transparent',
                drawBorder: false
            },
            ticks: {
                padding: yAxesPadding,
                fontColor: '#101010',
                fontStyle: 'bold',
                fontSize: 12
            }
        }],
        xAxes: [{
            gridLines: {
                display: true,
                drawTicks: false,
                color: 'transparent',
                zeroLineColor: '#C9C9C9'
            },
            ticks: {
                padding: 0,
                min: horizontalMinValue,
                max: horizontalMaxValue,
                callback(label: any, index: any, labels: any) {
                    return ' ';
                }
            }
        }]
    },
    hover: {
      animationDuration: 1
    },
    animation: {
      duration: 1,
      // tslint:disable-next-line: no-shadowed-variable
      onComplete(chartProps: any) {
          chartProps.chart.ctx.textAlign = 'center';
          chartProps.chart.ctx.textBaseline = 'top';

          horizontalChartData(horizontalChartIndex, horizontalColorsArr).forEach((dataset, i) => {
            chartProps.chart.controller.getDatasetMeta(i).data.forEach((bar: any, index: any) => {
              if (dataset.data[index] > 0) {
                chartProps.chart.ctx.fillText(dataset.data[index] + '%', bar._model.x + 18, bar._model.y - 5);
              } else {
                chartProps.chart.ctx.fillText(dataset.data[index] + '%', bar._model.x - 18, bar._model.y - 5);
              }
            });
          });
      }
    }
  };
};

export const horizontalChartData = (horizontalChartIndex: any[], horizontalColorsArr: any[]): any[] => {
  return [
    {
      data: horizontalChartIndex,
      backgroundColor: horizontalColorsArr,
      hoverBackgroundColor: horizontalColorsArr
    }
  ];
};
