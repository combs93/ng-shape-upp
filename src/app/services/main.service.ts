import { config } from './../app.config';
import { environment } from './../../environments/environment';
import { EtfContent, MainContent, SearchContent } from './../types/main.types';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Router, Params } from '@angular/router';

interface DirtyMainContentResponse {
  result: MainContent;
}
interface DirtySearchContentResponse {
  result: SearchContent;
}
interface GetMainContentPayload {
  code?: string;
  exchange?: string;
  chart?: string;
}

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) {}

  getMainContent(params: any, chart: string = 'YTD'): Observable<MainContent> {
    return this.http.post<DirtyMainContentResponse>(`${environment.host}/screener/info`, {...params, chart})
      .pipe(
        map(res => ({...res.result, params})),
        catchError(err => {
          // ! WARNING must be redirect to 404 page
          this.router.navigate(['/']);
          return of(null);
        })
      );
  }

  getEtfContent(params: any, chart: string = 'YTD'): Observable<EtfContent> {
    return this.http.post<DirtyMainContentResponse>(`${environment.host}/screener/info`, {...params, chart})
      .pipe(
        map(res => ({...res.result, params})),
        catchError(err => {
          // ! WARNING must be redirect to 404 page
          this.router.navigate(['/']);
          return of(null);
        })
      );
  }

  getQueryParamsForMainPage(params: Params): Params {
    let queryParams = config.defaultParams;
    
    if (params.code && params.exchange) {
      queryParams.code = params.code;
      queryParams.exchange = params.exchange;
    }
    
    return queryParams;
  }
  
  getSearchContent(val: any): Observable<SearchContent> {
    return this.http.post<DirtySearchContentResponse>(`${environment.host}/screener/search`, {code: val})
      .pipe(
        map(res => res.result),
      );
  }

  // START premium block features
  // closePremiumBlock(): void {
  //   localStorage.setItem('showPremiumBlock', 'false');
  // }

  // subscribePremium(): void {
  //   localStorage.setItem('subscribed', 'true');
  //   localStorage.setItem('showPremiumBlock', 'false');
  // }
  // END premium block features

}
