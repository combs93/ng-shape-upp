import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EtfScreenerComponent } from './pages/etf-screener/etf-screener.component';
import { EtfScreenerResolver } from './pages/etf-screener/etf-screener.resolver';
import { LandingComponent } from './pages/landing/landing.component';
import { LandingResolver } from './pages/landing/landing.resolver';
import { MainComponent } from './pages/main/main.component';
import { MainResolver } from './pages/main/main.resolver';


const routes: Routes = [
  {
    path: 'screener',
    component: MainComponent,
    resolve: {
      response: MainResolver
    }
  },
  {
    path: 'etfscreener',
    component: EtfScreenerComponent,
    resolve: {
      response: EtfScreenerResolver
    }
  },
  {
    path: '',
    component: LandingComponent,
    resolve: {
      response: LandingResolver
    }
  },
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
