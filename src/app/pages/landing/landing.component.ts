import { ActivatedRoute } from '@angular/router';
import { Component, OnInit  } from '@angular/core';
import { MainService } from 'src/app/services/main.service';
import { skip } from 'rxjs/operators';
import { MainContent } from 'src/app/types/main.types';
import { MainChartService } from 'src/app/components/main-chart/main-chart.service';

@Component({
  templateUrl: './landing.component.html'
})
export class LandingComponent implements OnInit {

  mainPageInfo: MainContent;
  urlParams: any;
  benchmarkData: any;
  riskData: any;
  dividendData: any;
  valuationData: any;
  chartData: any;

  constructor(
    private route: ActivatedRoute,
    private mainChartService: MainChartService,
    private mainService: MainService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.parent.queryParams.pipe(skip(1)).subscribe(params => {
      this.updateMainInfo(params);
    });

    this.mainPageInfo = this.route.snapshot.data.response;
    console.log('mainPageInfo - ', this.mainPageInfo);

    this.mainChartService.setPriceChart(this.mainPageInfo.chart?.price);
    this.mainChartService.setEpsChart(this.mainPageInfo.chart?.earnings_per_share);
    // this.mainChartService.setVectors(this.mainPageInfo.chart.vector);

    this.benchmarkData = this.setBenchmarkData();
    this.riskData = this.setRiskData();
    this.dividendData = this.setDividendData();
    this.valuationData = this.setValuationData();
    this.chartData = this.setChartData();
  }

  updateMainInfo(params: any) {
    this.mainService.getMainContent(params).subscribe(res => {
      this.mainPageInfo = res;
      this.mainChartService.setPriceChart(res.chart?.price);
      this.mainChartService.setEpsChart(res.chart?.earnings_per_share);

      this.benchmarkData = this.setBenchmarkData();
      this.riskData = this.setRiskData();
      this.dividendData = this.setDividendData();
      this.valuationData = this.setValuationData();
      this.chartData = this.setChartData();
    });
  }

  setBenchmarkData() {
    return {...this.mainPageInfo?.benchmark_index, ...this.mainPageInfo?.params};
  }

  setRiskData() {
    return {...this.mainPageInfo?.risk, ...this.mainPageInfo?.params};
  }

  setDividendData() {
    return {chartData: {...this.mainPageInfo?.dividend}, paramsData: {...this.mainPageInfo?.params}};
  }

  setValuationData() {
    return {...this.mainPageInfo?.valuation, ...this.mainPageInfo?.params};
  }

  setChartData() {
    return {...this.mainPageInfo?.chart, ...this.mainPageInfo?.params};
  }

  

}
