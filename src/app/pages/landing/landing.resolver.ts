import { MainContent } from './../../types/main.types';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { MainService } from 'src/app/services/main.service';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})

export class LandingResolver implements Resolve<MainContent> {

  constructor(
    private mainService: MainService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MainContent>  {
    const queryParams = {code: 'AAPL', exchange: 'US'};

    return this.mainService.getMainContent(queryParams);
  }

}
