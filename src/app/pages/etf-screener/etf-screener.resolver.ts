import { MainContent, SearchContent } from './../../types/main.types';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { MainService } from 'src/app/services/main.service';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})

export class EtfScreenerResolver implements Resolve<MainContent> {

  constructor(
    private mainService: MainService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>  {
    const queryParams = this.mainService.getQueryParamsForMainPage(route.queryParams);

    return this.mainService.getEtfContent(queryParams);
  }

}
