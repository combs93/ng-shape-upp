import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, timer } from 'rxjs';
import { exhaustMap, filter, map, takeUntil } from 'rxjs/operators';
import { MainChartService } from 'src/app/components/main-chart/main-chart.service';
import { MainService } from 'src/app/services/main.service';
import { EtfContent } from 'src/app/types/main.types';

@Component({
  templateUrl: './etf-screener.component.html'
})
export class EtfScreenerComponent implements OnInit {

  etfPageInfo: EtfContent;
  urlParams: any;
  benchmarkData: any;
  generalData: any;
  valuationData: any;
  chartData: any;
  riskAdjustedData: any;
  longTermData: any;
  yieldData: any;
  riskData: any;
  doughnutData: any;
  barsData: any;

  constructor(
    private route: ActivatedRoute,
    private mainChartService: MainChartService,
    private mainService: MainService,
  ) { }

  ngOnInit() {
    this.route.parent.queryParams.subscribe(params => {
      this.updateMainInfo(params);
    });
    
    this.etfPageInfo = this.route.snapshot.data.response;
    console.log('etfPageInfo - ', this.etfPageInfo);

    this.mainChartService.setPriceChart(this.etfPageInfo.chart?.price);
    this.mainChartService.setEpsChart(this.etfPageInfo.chart?.earnings_per_share);
    // this.mainChartService.setVectors(this.etfPageInfo.chart.vector);

    this.generalData = this.setGeneralData();
    this.benchmarkData = this.setBenchmarkData();
    this.valuationData = this.setValuationData();
    this.chartData = this.setChartData();
    this.riskAdjustedData = this.setRiskAdjustedData();
    this.longTermData = this.setLongTermData();
    this.yieldData = this.setYieldData();
    this.riskData = this.setRiskData();
    this.doughnutData = this.setDoughnutData();
    this.barsData = this.setBarsData();
    console.log('this.doughnutData - ', this.doughnutData)
  }

  updateMainInfo(params: any) {
    const isLoaded = new Subject();
    timer(0, 1000).pipe(takeUntil(isLoaded), exhaustMap(() => this.mainService.getEtfContent(params).pipe(map(res => {
      if (Object.keys(res).length >= 16) {
        isLoaded.next();
        isLoaded.complete();
      }
      this.etfPageInfo = res;
      this.mainChartService.setPriceChart(res.chart?.price);
      this.mainChartService.setEpsChart(res.chart?.earnings_per_share);

      this.generalData = this.setGeneralData();
      this.benchmarkData = this.setBenchmarkData();
      this.valuationData = this.setValuationData();
      this.chartData = this.setChartData();
      this.riskAdjustedData = this.setRiskAdjustedData();
      this.longTermData = this.setLongTermData();
      this.yieldData = this.setYieldData();
      this.riskData = this.setRiskData();
      this.doughnutData = this.setDoughnutData();
      this.barsData = this.setBarsData();

    })))).subscribe();
  }

  setGeneralData() {
    return {...this.etfPageInfo?.general, ...this.etfPageInfo?.etf, ...this.etfPageInfo?.etf_morningstar};
  }

  setBenchmarkData() {
    return {...this.etfPageInfo?.benchmark_index, ...this.etfPageInfo?.params};
  }

  setValuationData() {
    let valuationChartData = {'stock': [], 'index': []};

    if (this.etfPageInfo?.etf_valuations_growth && this.etfPageInfo?.etf_valuations_growth.length > 0) {
      this.etfPageInfo?.etf_valuations_growth.forEach(item => {
        if (item.type === 'Valuations_Rates_Portfolio') {
          valuationChartData['stock'].push(item.price_prospective_earnings);
        } else if (item.type === 'Growth_Rates_Portfolio') {
          valuationChartData['stock'].push(item.price_prospective_earnings);
        } else if (item.type === 'Valuations_Rates_To_Category') {
          valuationChartData['index'].push(item.price_prospective_earnings);
        } else if (item.type === 'Growth_Rates_To_Category') {
          valuationChartData['index'].push(item.price_prospective_earnings);
        }
      });
    }
    return {...valuationChartData, ...this.etfPageInfo?.params};
  }

  setChartData() {
    return {...this.etfPageInfo?.chart, ...this.etfPageInfo?.params};
  }

  setRiskAdjustedData() {
    return {...this.etfPageInfo?.etf_performance, ...this.etfPageInfo?.params};
  }

  setLongTermData() {
    let valuationGrowth = {};
    if (this.etfPageInfo?.etf_valuations_growth && this.etfPageInfo?.etf_valuations_growth.length > 0) {
      this.etfPageInfo?.etf_valuations_growth.forEach(item => {
        if (item.type === 'Growth_Rates_Portfolio') {
          valuationGrowth[this.etfPageInfo?.params.code] = +item.long_term_projected_earnings_growth
        } else if (item.type === 'Growth_Rates_To_Category') {
          valuationGrowth['Category'] = +item.long_term_projected_earnings_growth
        }
      });
    }
    return {valuationGrowth, ...this.etfPageInfo?.params};
  }

  setYieldData() {
    let yieldChartData = {};
    if (this.etfPageInfo?.etf) {
      yieldChartData[this.etfPageInfo?.params.code] = +this.etfPageInfo?.etf.yield;
    }
    if (this.etfPageInfo?.etf_valuations_growth && this.etfPageInfo?.etf_valuations_growth.length > 0) {
      this.etfPageInfo?.etf_valuations_growth.forEach(item => {
        if (item.type === 'Growth_Rates_To_Category') {
          yieldChartData['Category'] = +item.long_term_projected_earnings_growth;
        }
      });
    }
    console.log({yieldChartData, ...this.etfPageInfo?.params})
    return {yieldChartData, ...this.etfPageInfo?.params};
  }

  setRiskData() {
    let riskChartData = {};

    if (typeof this.etfPageInfo?.etf_performance !== "undefined" && this.etfPageInfo?.etf_performance !== null) {
      riskChartData['etf'] = {
        standard_deviation_stock: this.etfPageInfo?.etf_performance.volatility_3y ? this.etfPageInfo?.etf_performance.volatility_3y : this.etfPageInfo?.etf_performance.volatility_1y,
        beta_stock: this.etfPageInfo?.technical.beta 
      }
    }

    return {...riskChartData, ...this.etfPageInfo?.params};
  }

  setDoughnutData() {
    let doughnutData = {'asset': {'name': [], 'data': []}, 'world': {'name': [], 'data': []}};
    let sortedGeography;
    let filteredGeography;
    let sortedAsset;
    let filteredAsset;

    if (this.etfPageInfo?.etf_world_region && this.etfPageInfo?.etf_world_region.length > 0) {
      sortedGeography = this.etfPageInfo?.etf_world_region.sort((a, b) => {
        return a.equity_percent - b.equity_percent
      })
      filteredGeography = sortedGeography.reverse().splice(0, 5);
      
      filteredGeography.forEach((item, index) => {
        doughnutData.world.name.push(item.type);
        doughnutData.world.data.push(item.equity_percent > 0 ? item.equity_percent.toFixed(1) : item.equity_percent);
      });
    }
    
    if (this.etfPageInfo?.etf_allocation && this.etfPageInfo?.etf_allocation.length > 0) {
      sortedAsset = this.etfPageInfo?.etf_allocation.sort((a, b) => {
        return a.net_assets_percent - b.net_assets_percent
      })
      filteredAsset = sortedAsset.reverse().splice(0, 5);
      
      filteredAsset.forEach((item, index) => {
        doughnutData.asset.name.push(item.type);
        doughnutData.asset.data.push(item.net_assets_percent > 0 ? item.net_assets_percent.toFixed(1) : item.net_assets_percent);
      });
    }
    
    return {doughnutData}
  }

  setBarsData() {
    let sortedHoldings;
    let filteredHoldings;
    let sortedSector;
    let filteredSector;
    let capitalisationObj;
    let capitalisationObg;

    if (this.etfPageInfo?.etf_holding && this.etfPageInfo?.etf_holding.length > 0) {
      sortedHoldings = this.etfPageInfo?.etf_holding.sort((a, b) => {
        return a.assets_percent - b.assets_percent
      })
      filteredHoldings = sortedHoldings.length > 9 ? sortedHoldings.reverse().splice(0, 10) : sortedHoldings.reverse();
    }

    if (this.etfPageInfo?.etf_sector_weight && this.etfPageInfo?.etf_sector_weight.length > 0) {
      sortedSector = this.etfPageInfo?.etf_sector_weight.sort((a, b) => {
        return a.equity_percent - b.equity_percent
      })
      filteredSector = sortedSector.length > 9 ? sortedSector.reverse().splice(0, 10) : sortedSector.reverse();
    }

    if (typeof this.etfPageInfo?.etf_performance !== "undefined" && this.etfPageInfo?.etf_performance !== null) {
      capitalisationObj = this.etfPageInfo?.etf_capitalisation;
      capitalisationObg = {'mega': capitalisationObj.mega, 'big': capitalisationObj.big, 'medium': capitalisationObj.medium, 'small': capitalisationObj.small, 'micro': capitalisationObj.micro};
    }
    
    return {'holdings': filteredHoldings, 'sector': filteredSector, 'capitalisation': capitalisationObg}
  }

}
