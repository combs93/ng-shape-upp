import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MainComponent } from './pages/main/main.component';
import { PremiumVersionComponent } from './components/premium-version/premium-version.component';
import { CompanyInfoComponent } from './components/company-info/company-info.component';
import { AnalystPriceComponent } from './components/analyst-price/analyst-price.component';
import { PriceChartComponent } from './components/price-chart/price-chart.component';
import { ValuationChartComponent } from './components/valuation-chart/valuation-chart.component';
import { DividentChartComponent } from './components/divident-chart/divident-chart.component';
import { GrowthChartComponent } from './components/growth-chart/growth-chart.component';
import { ProfitabilityChartComponent } from './components/profitability-chart/profitability-chart.component';
import { RiskChartComponent } from './components/risk-chart/risk-chart.component';
import { SubscriptionFormComponent } from './components/subscription-form/subscription-form.component';
import { BenchmarkIndexComponent } from './components/benchmark-index/benchmark-index.component';
import { EarningsChartComponent } from './components/earnings-chart/earnings-chart.component';
import { MainChartComponent } from './components/main-chart/main-chart.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchFormPipePipe } from './components/search-form/search-form-pipe.pipe';
import { PreloaderComponent } from './components/preloader/preloader.component';
import { AmountPipe } from './pipes/amount.pipe';
import { LandingComponent } from './pages/landing/landing.component';
import { BannerComponent } from './components/banner/banner.component';
import { BenefitsComponent } from './components/benefits/benefits.component';
import { EtfScreenerComponent } from './pages/etf-screener/etf-screener.component';
import { AllocationChartsComponent } from './components/etf/allocation-charts/allocation-charts.component';
import { BarChartsComponent } from './components/etf/bar-charts/bar-charts.component';
import { RiskAdjustedChartComponent } from './components/etf/risk-adjusted-chart/risk-adjusted-chart.component';
import { LongTernChartComponent } from './components/etf/long-tern-chart/long-tern-chart.component';
import { YieldChartComponent } from './components/etf/yield-chart/yield-chart.component';
import { EftBenchmarkComponent } from './components/etf/eft-benchmark/eft-benchmark.component';
import { EtfValuationComponent } from './components/etf/etf-valuation/etf-valuation.component';
import { EtfRiskComponent } from './components/etf/etf-risk/etf-risk.component';
import { EtfPriceComponent } from './components/etf-price/etf-price.component';
import { AllocationDoughnutComponent } from './components/etf/allocation-doughnut/allocation-doughnut.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { TermsOfServiceComponent } from './components/dialogs/terms-of-service/terms-of-service.component';
import { PrivacyPolicyComponent } from './components/dialogs/privacy-policy/privacy-policy.component';
import { SuccessDialogComponent } from './components/dialogs/success-dialog/success-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { EtfCompanyInfoComponent } from './components/etf/etf-company-info/etf-company-info.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    PremiumVersionComponent,
    CompanyInfoComponent,
    AnalystPriceComponent,
    PriceChartComponent,
    ValuationChartComponent,
    DividentChartComponent,
    GrowthChartComponent,
    ProfitabilityChartComponent,
    RiskChartComponent,
    SubscriptionFormComponent,
    BenchmarkIndexComponent,
    EarningsChartComponent,
    MainChartComponent,
    SearchFormComponent,
    SearchFormPipePipe,
    PreloaderComponent,
    AmountPipe,
    LandingComponent,
    BannerComponent,
    BenefitsComponent,
    EtfScreenerComponent,
    AllocationChartsComponent,
    BarChartsComponent,
    RiskAdjustedChartComponent,
    LongTernChartComponent,
    YieldChartComponent,
    EftBenchmarkComponent,
    EtfValuationComponent,
    EtfRiskComponent,
    EtfPriceComponent,
    AllocationDoughnutComponent,
    TermsOfServiceComponent,
    PrivacyPolicyComponent,
    SuccessDialogComponent,
    EtfCompanyInfoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatFormFieldModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
