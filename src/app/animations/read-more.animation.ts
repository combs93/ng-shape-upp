import { trigger, state, style, transition, animate } from '@angular/animations';

export const readMoreAnimate = trigger('slideToggle', [
  state('in', style({
    height: '*'
  })),
  state('out', style({
    height: '34px'
  })),
  transition('in => out', animate('400ms ease-in-out')),
  transition('out => in', animate('400ms ease-in-out'))
]);
