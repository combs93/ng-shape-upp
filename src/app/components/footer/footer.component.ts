import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PrivacyPolicyComponent } from '../dialogs/privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from '../dialogs/terms-of-service/terms-of-service.component';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {

  listItems: string[] = ['FAQ', 'Blog', 'Terms of Service', 'Privacy Policy'];

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openPrivacyPolicyDialog() {
    const dialogRef = this.dialog.open(PrivacyPolicyComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  
  openTermsOfServiceDialog() {
    const dialogRef = this.dialog.open(TermsOfServiceComponent);
  
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
