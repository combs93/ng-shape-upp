import { Component, OnInit, ViewChild, Input, ElementRef, OnDestroy, OnChanges } from '@angular/core';
declare var Chart: any;

let generalValuationChart = [];

@Component({
  selector: 'app-valuation-chart',
  templateUrl: './valuation-chart.component.html'
})
export class ValuationChartComponent implements OnInit, OnDestroy, OnChanges {

  @Input() valuationChart: any;
  @ViewChild('valuationChart', {static: true}) chartElemementRef: ElementRef;

  private chartInstance: any;

  chartColorRed = 'rgba(185,69,82,1)';
  chartColorGreen = 'rgba(16,167,95,1)';
  chartColorBlue = 'rgba(16,68,167,1)';

  valuationChartLabels = ['Current Price/Earnings', 'Forward Price/Earnings'];

  valuationChartType = 'bar';

  valuationChartStock = [];
  valuationChartIndex = [];
  commonValuesArr = [];

  valuationMinValue: number;
  valuationMaxValue: number;
  yAxesPadding: number;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.createChartData();
    this.initValuationChart();
  }

  ngOnDestroy() {
    this.resetChartInstance();
  }

  createChartData() {
    generalValuationChart = [Object.values(this.valuationChart.stock), Object.values(this.valuationChart.index)];

    this.valuationChartStock = Object.values(this.valuationChart.stock).map(el => (typeof el) === 'number' ? el : 0);
    this.valuationChartIndex = Object.values(this.valuationChart.index).map(el => (typeof el) === 'number' ? el : 0);

    this.commonValuesArr = this.valuationChartStock.concat(this.valuationChartIndex);

    this.valuationMinValue = Math.min(...this.commonValuesArr) < 0 ? Math.min(...this.commonValuesArr) : 0;
    this.valuationMaxValue = Math.max(...this.commonValuesArr) > 100 ? Math.max(...this.commonValuesArr) : 100;
    // this.valuationMaxValue = 100;
    this.yAxesPadding = Math.min(...this.commonValuesArr) < 0 ? 50 : 0;
  }

  private initValuationChart() {
    this.resetChartInstance();

    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: this.valuationChartType,
      data: {
        labels: this.valuationChartLabels,
        borderSkipped: 'top',
        datasets: [
          {
              label: this.valuationChart.code,
              backgroundColor: this.chartColorGreen,
              hoverBackgroundColor: this.chartColorGreen,
              data: this.valuationChartStock
          },
          {
              label: 'Index',
              backgroundColor: this.chartColorBlue,
              hoverBackgroundColor: this.chartColorBlue,
              data: this.valuationChartIndex
          }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
          enabled: false
        },
        legend: {
          display: false
        },
        layout: {
            padding: {
              left: 0,
              right: 0,
              top: 30,
              bottom: 0
            }
        },
        scales: {
            yAxes: [{
                display: true,
                gridLines: {
                  color: 'transparent',
                  zeroLineColor: '#C9C9C9',
                  drawTicks: false,
                  drawBorder: false
                },
                ticks: {
                  padding: this.yAxesPadding,
                  fontStyle: 'bold',
                  min: this.valuationMinValue,
                  max: this.valuationMaxValue,
                  callback(value: any, index: any, labels: any) {
                      return ' ';
                  }
                }
            }],
            xAxes: [{
                barThickness: 30,
                gridLines: {
                  display: false,
                  drawTicks: false,
                  color: 'transparent',
                  drawBorder: false
                },
                ticks: {
                  padding: 50,
                  fontColor: '#6F6E6E',
                  fontSize: 11,
                  callback (label: any, index: any, labels: any) {
                    if (/\s/.test(label)) {
                      return label.split(' ');
                    } else{
                      return label;
                    }
                  }
                }
            }]
        },
        hover: {
          animationDuration: 1
        },
        animation: {
          duration: 1,
          onComplete() {
            const chartInstance = this.chart;
            const ctx = chartInstance.ctx;

            ctx.textAlign = 'center';
            ctx.textBaseline = 'top';

            this.data.datasets.forEach(function(dataset: any, i: number) {
              const meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function(bar: any, index: any) {
                const data = dataset.data[index];

                if (data > 0) {
                  ctx.fillText(typeof generalValuationChart[i][index] === 'number' ? generalValuationChart[i][index].toFixed(2) : 'N/A', bar._model.x, bar._model.y - 15);
                  ctx.fillText(dataset.label, bar._model.x, 250);
                } else if (data < 0) {
                  ctx.fillText(typeof generalValuationChart[i][index] === 'number' ? generalValuationChart[i][index].toFixed(2) : 'N/A', bar._model.x, bar._model.y + 10);
                  ctx.fillText(dataset.label, bar._model.x, 250);
                } else {
                  ctx.fillText(typeof generalValuationChart[i][index] === 'number' ? generalValuationChart[i][index] : 'N/A', bar._model.x, bar._model.y - 15);
                  ctx.fillText(dataset.label, bar._model.x, 250);
                }
              });
            });
          }
        }
      }
    });
  }

  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }

}
