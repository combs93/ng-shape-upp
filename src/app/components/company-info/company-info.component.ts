import { Component, OnInit, Input } from '@angular/core';
import { readMoreAnimate } from 'src/app/animations/read-more.animation';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-company-info',
  templateUrl: './company-info.component.html',
  animations: [readMoreAnimate]
})
export class CompanyInfoComponent implements OnInit {

  @Input() companyInfo: any;
  @Input() etfStuff: boolean;

  readMore: string;
  readMoreBtnText = 'Read more...';

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.readMore = 'out';
  }

  toggleHelpMenu(): void {
    this.readMore = this.readMore === 'out' ? 'in' : 'out';
    this.readMoreBtnText = this.readMore === 'out' ? 'Read more...' : 'Hide text';
  }

}
