import { MainService } from 'src/app/services/main.service';
import { Component, OnInit, HostListener, Input } from '@angular/core';

@Component({
  selector: 'app-premium-version',
  templateUrl: './premium-version.component.html'
})
export class PremiumVersionComponent implements OnInit {

  @Input() etfStuff: boolean;

  visibleState = false;
  // showPremiumBlock = true;

  constructor(
    private mainService: MainService
  ) { }

  ngOnInit(): void {
    // premium block status
    // if (localStorage.getItem('showPremiumBlock')) {
    //   this.showPremiumBlock = !localStorage.getItem('showPremiumBlock');
    // }
  }

  // premium block features
  // subscribePremium() {
  //   this.mainService.subscribePremium();
  //   this.showPremiumBlock = false;
  // }

  @HostListener('window:scroll')
  fixedPremiumBlock() {
    const limitedHeight = 200;
    const scrollY = window.scrollY;
    this.visibleState = scrollY > limitedHeight;
  }
}
