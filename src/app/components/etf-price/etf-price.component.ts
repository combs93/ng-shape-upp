import { MainChartService } from './../main-chart/main-chart.service';
import { Component, OnInit, Input, ElementRef, ViewChild, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { priceChartData, priceChartOptions } from '../price-chart/price-chart.utils';
declare var Chart: any;

@Component({
  selector: 'app-etf-price',
  templateUrl: './etf-price.component.html'
})
export class EtfPriceComponent implements OnInit, OnDestroy, OnChanges {

  @Input() spyIsActive: boolean;
  @Input() queryParams: any;
  @Input() priceInfo: any;
  @ViewChild('priceChart', {static: true}) chartElemementRef: ElementRef;

  private chartInstance: any;

  chartColorRed = 'rgba(185,69,82,1)';
  chartColorGreen = 'rgba(16,167,95,1)';
  chartColorBlue = 'rgba(16,68,167,1)';

  chartArr = [4, 12, 15, 7, -3, 0, 5, 2, 2, 10, 8, 11, -5, 2, -7, 5, 11];
  chartArrClose: string[];
  indexArr: number[];
  indexArrClose: string[];

  priceChartOptions: any;
  priceChartData: any;
  priceChartLabels: string[];
  priceChartType = 'line';

  stockVectorState = true;
  indexVectorState = true;


  constructor(
    private mainChartService: MainChartService
  ) {}

  ngOnInit(): void {
    this.initPriceChart('rgba(16,167,95,1)','rgba(16,68,167,1)')
  }

  ngOnDestroy(): void {
    this.resetChartInstance();
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  private initPriceChart(greenColor: string, blueColor: string) {
    Chart.defaults.priceChart = Chart.helpers.clone(Chart.defaults.line);
    Chart.controllers.priceChart = Chart.controllers.line.extend({
      update: function () {
        this.chart.data.datasets.forEach(element => {
          let min = Math.min.apply(null, element.data);
          let max = Math.max.apply(null, element.data);
          let yScale = this.getScaleForId(this.getMeta().yAxisID);
  
          let top = yScale.getPixelForValue(max);
          let zero = yScale.getPixelForValue(0);
          let bottom = yScale.getPixelForValue(min);
  
          let ctx = this.chart.chart.ctx;
          let ratio = Math.min((zero - top) / (bottom - top), 1);
  
          let gradient = ctx.createLinearGradient(0, top, 0, bottom);
          let line_gradient = ctx.createLinearGradient(0, top, 0, bottom);
          line_gradient.addColorStop(0, element.borderDash ? 'rgba(16,68,167,1)' : 'rgba(16,167,95,1)');
          line_gradient.addColorStop(ratio, element.borderDash ? 'rgba(16,68,167,1)' : 'rgba(16,167,95,1)');
          line_gradient.addColorStop(ratio, 'rgba(185,69,82,1)');
          line_gradient.addColorStop(1, 'rgba(185,69,82,1)');
  
          element.backgroundColor = gradient;
          element.borderColor = line_gradient;
          element.pointBackgroundColor = line_gradient;
  
          return Chart.controllers.line.prototype.update.apply(this, arguments)
        });
      }
    });


    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: 'priceChart',
      data: {
          labels: ["Jan 7, 2020", "Jan 7, 2020", "Jan 17, 2020", "Jan 17, 2020", "Jan 23, 2020", "Feb 7, 2020", "Feb 7, 2020", "Feb 7, 2020", "Feb 7, 2020", "Feb 7, 2020", "Feb 7, 2020", "Feb 7, 2020", "Feb 7, 2020", "Feb 7, 2020", "Feb 7, 2020", "Feb 7, 2020", "Feb 29, 2020"],
          legend: {
              display: false
          },
          datasets: [
            {
              data: [4, 12, 15, 7, -3, 0, 5, 2, 2, 10, 8, 11, -5, 2, -7, 5, 11],
              pointBackgroundColor: this.chartColorGreen,
              backgroundColor: this.chartColorGreen,
              borderColor: this.chartColorGreen,
              borderWidth: ['2'],
              fill: false
            }
          ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
          enabled: true
        },
        hover: {mode: null},
        legend: {
            display: false
        },
        elements: {
            line: {
                tension: 0
            },
            point:{
                radius: 2
            }
        },
        scales: {
          yAxes: [{
            display: true,
            position: 'right',
            gridLines: {
                color: 'transparent',
                zeroLineColor: 'rgba(110, 110, 110, 1)',
                zeroLineBorderDash: [6, 4]
            },
            ticks: {
                beginAtZero: true,
                autoSkip: false,
                fontStyle: 'bold',
                stepSize: 0.01,
                callback(value: any, index: any, values: any) {
                    if (this.chartArr && this.chartArr.length && value === this.chartArr[this.chartArr.length - 1]) {
                        return this.chartArr[this.chartArr.length - 1] > 0 ? '+' + value + '%' : ' ' + value + '%';
                    } else if (this.indexArr && this.indexArr.length && value === this.indexArr[this.indexArr.length - 1]) {
                        return this.indexArr[this.indexArr.length - 1] > 0 ? '+' + value + '%' : ' ' + value + '%';
                    } else if (value === 0) {
                        return value;
                    } else {
                        return null;
                    }
                }
            }
          }],
          xAxes: [{
            stacked: false,
            gridLines: {
                color: 'transparent',
                zeroLineColor: 'transparent'
            },
            ticks: {
                autoSkip: true,
                padding: 10,
                maxTicksLimit: 6,
                fontSize: '10',
                maxRotation: 0,
                callback(value: any, index: any, values: any) {
                    return value;
                }
            }
          }]
        }
      }
    });
  }
  
  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }

}