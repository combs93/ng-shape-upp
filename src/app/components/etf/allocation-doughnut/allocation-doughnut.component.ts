import { Component, OnInit, ViewChild, ElementRef, OnDestroy, OnChanges, Input } from '@angular/core';
declare var Chart: any;
@Component({
  selector: 'app-allocation-doughnut',
  templateUrl: './allocation-doughnut.component.html'
})
export class AllocationDoughnutComponent implements OnInit, OnDestroy, OnChanges {

  @ViewChild('allocationChart', {static: true}) chartElemementRef: ElementRef;
  @Input() chartData: any;

  private chartInstance: any;

  constructor() { }

  ngOnInit(): void {
    this.initRiskChart();
  }
  
  ngOnChanges() {
  }
  
  ngOnDestroy() {
    this.resetChartInstance();
  }

  private initRiskChart() {
    this.resetChartInstance();

    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: 'doughnut',
      data: {
        labels: this.chartData.name,
        datasets: [{
            data: this.chartData.data,
            backgroundColor: [
                'rgba(60, 128, 236, 1)',
                'rgba(87, 139, 222, 1)',
                'rgba(129, 166, 224, 1)',
                'rgba(181, 202, 236, 1)',
                'rgba(221, 234, 255, 1)'
            ],
            hoverBackgroundColor: [
              'rgba(60, 128, 236, .7)', 
              'rgba(87, 139, 222, .7)', 
              'rgba(129, 166, 224, .7)', 
              'rgba(181, 202, 236, .7)', 
              'rgba(221, 234, 255, .7)'
            ],
            borderColor: [
              'rgba(60, 128, 236, 1)',
              'rgba(87, 139, 222, 1)',
              'rgba(129, 166, 224, 1)',
              'rgba(181, 202, 236, 1)',
              'rgba(221, 234, 255, 1)'
            ],
            hoverBorderColor: [
              'rgba(60, 128, 236, .7)', 
              'rgba(87, 139, 222, .7)', 
              'rgba(129, 166, 224, .7)', 
              'rgba(181, 202, 236, .7)', 
              'rgba(221, 234, 255, .7)'
            ],
            borderWidth: 1
        }]
      },
      options: {
        animation: {
            animateScale: true,
            animateRotate: true
          },
        responsive: true,
        maintainAspectRatio: false,
        cutoutPercentage: 70,
        legend: {
          display: false
        },
      }
    })
  }

  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }

}
