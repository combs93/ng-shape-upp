import { Component, OnInit, Input } from '@angular/core';
import { readMoreAnimate } from 'src/app/animations/read-more.animation';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-etf-company-info',
  templateUrl: './etf-company-info.component.html',
  animations: [readMoreAnimate]
})
export class EtfCompanyInfoComponent implements OnInit {

  @Input() companyInfo: any;

  readMore: string;
  readMoreBtnText = 'Read more...';

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    console.log('------------ ', this.companyInfo)
    this.readMore = 'out';
  }

  toggleHelpMenu(): void {
    this.readMore = this.readMore === 'out' ? 'in' : 'out';
    this.readMoreBtnText = this.readMore === 'out' ? 'Read more...' : 'Hide text';
  }

}