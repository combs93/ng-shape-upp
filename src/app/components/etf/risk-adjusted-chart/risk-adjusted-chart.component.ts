import { Component, OnInit, ViewChild, Input, ElementRef, OnDestroy, OnChanges } from '@angular/core';
declare var Chart: any;

let generalRiskAdjustedChartArr = [];

@Component({
  selector: 'app-risk-adjusted-chart',
  templateUrl: './risk-adjusted-chart.component.html'
})
export class RiskAdjustedChartComponent implements OnInit, OnDestroy, OnChanges {
  @Input() riskAdjustedData: any;
  @ViewChild('riskAdjustedChart', {static: true}) chartElemementRef: ElementRef;

  private chartInstance: any;

  chartColorRed = 'rgba(185,69,82,1)';
  chartColorGreen = 'rgba(16,167,95,1)';
  chartColorBlue = 'rgba(16,68,167,1)';

  riskAdjustedChartColorsArr = [];

  riskAdjustedChartType = 'horizontalBar';

  riskAdjustedChartArr = [];

  riskAdjustedMinValue: number;
  riskAdjustedMaxValue: number;
  yAxesPadding: number;

  riskAdjustedChartLabels = [];

  riskAdjustedChartLegend = false;


  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.createChartData();
    this.initRiskAdjustedChart();
  }

  ngOnDestroy() {
    this.resetChartInstance();
  }

  createChartData() {
    generalRiskAdjustedChartArr = [this.riskAdjustedData.sharp_ratio_3y];

    this.riskAdjustedChartArr = generalRiskAdjustedChartArr.map(el => (typeof el) === 'number' ? el : 0);

    this.riskAdjustedChartColorsArr = [];

    this.riskAdjustedChartArr.forEach((el, index) => {
      el >= 0 ? this.riskAdjustedChartColorsArr.push(this.chartColorGreen) : this.riskAdjustedChartColorsArr.push(this.chartColorRed);
    });
    
    this.riskAdjustedChartLabels = [];
    this.riskAdjustedChartLabels.push(this.riskAdjustedData.code);

    this.riskAdjustedMinValue = Math.min(...this.riskAdjustedChartArr) < 0 ? Math.min(...this.riskAdjustedChartArr) : 0;
    this.riskAdjustedMaxValue = Math.max(...this.riskAdjustedChartArr) > 100 ? Math.max(...this.riskAdjustedChartArr) : 100;
    this.yAxesPadding = Math.min(...this.riskAdjustedChartArr) < 0 ? 60 : 0;
  }

  private initRiskAdjustedChart() {
    this.resetChartInstance();

    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: this.riskAdjustedChartType,
      data: {
          labels: this.riskAdjustedChartLabels,
          datasets: [
            {
              data: this.riskAdjustedChartArr,
              backgroundColor: this.riskAdjustedChartColorsArr,
              hoverBackgroundColor: this.riskAdjustedChartColorsArr
            }
          ]
      },
      options: {
          responsive: true,
          maintainAspectRatio: false,
          tooltips: {
              enabled: false
          },
          legend: {
              display: false
          },
          layout: {
              padding: {
                  left: 0,
                  right: 70,
                  top: 0,
                  bottom: 0
              }
          },
          scales: {
              yAxes: [{
                  display: true,
                  scaleLabel: {
                    align: 'end'
                  },
                  barThickness: 30,
                  gridLines: {
                      color: 'transparent',
                      drawBorder: false
                  },
                  ticks: {
                      padding: this.yAxesPadding,
                      fontColor: '#101010',
                      fontStyle: 'bold',
                      fontSize: 12
                  }
              }],
              xAxes: [{
                  scaleLabel: {
                    align: 'end'
                  },
                  gridLines: {
                      display: true,
                      drawTicks: false,
                      drawBorder: true,
                      color: 'transparent',
                      zeroLineColor: '#C9C9C9'
                  },
                  ticks: {
                      padding: 0,
                      min: this.riskAdjustedMinValue,
                      max: this.riskAdjustedMaxValue,
                      callback: function(label: any, index: any, labels: any) {
                          return ' ';
                      }
                  }
              }]
          },
          hover: {
            animationDuration: 1
          },
          animation: {
            duration: 1,
            onComplete: function() {
              const chartInstance = this.chart;
              const ctx = chartInstance.ctx;
              let dataArr = [];

              ctx.textAlign = 'center';
              ctx.textBaseline = 'top';

              generalRiskAdjustedChartArr.forEach(function(dataset: any, i: number) {
                typeof dataset === 'number' ? dataArr.push(dataset) : dataArr.push('N/A');
              });

              this.data.datasets.forEach(function(dataset: any, i: number) {
                let meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function(bar: any, index: any) {
                  let data = dataset.data[index];
                  if (data > 0) {
                      ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index].toFixed(2) + '%') : dataArr[index], bar._model.x + 30, bar._model.y - 5);
                  } else if (data < 0) {
                      ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index].toFixed(2) + '%') : dataArr[index], bar._model.x - 30, bar._model.y - 5);
                  } else {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index] + '%') : dataArr[index], bar._model.x + 30, bar._model.y - 5);
                  }
                });
              });
            }
          }
      }
    })
  }

  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }

}