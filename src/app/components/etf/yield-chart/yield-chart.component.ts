import { Component, OnInit, ViewChild, Input, ElementRef, OnDestroy, OnChanges } from '@angular/core';
declare var Chart: any;

let generalYieldChartArr = [];

@Component({
  selector: 'app-yield-chart',
  templateUrl: './yield-chart.component.html'
})
export class YieldChartComponent implements OnInit, OnDestroy, OnChanges {
  @Input() yieldData: any;
  @ViewChild('yieldChart', {static: true}) chartElemementRef: ElementRef;

  private chartInstance: any;

  chartColorRed = 'rgba(185,69,82,1)';
  chartColorGreen = 'rgba(16,167,95,1)';
  chartColorBlue = 'rgba(16,68,167,1)';

  yieldChartColorsArr = [];

  yieldChartType = 'horizontalBar';

  yieldChartArr = [];

  profitabilityMinValue: number;
  profitabilityMaxValue: number;
  yAxesPadding: number;

  yieldChartLabels = [];

  yieldChartLegend = false;


  constructor() { }

  ngOnInit(): void {
    console.log(this.yieldData)
  }

  ngOnChanges() {
    this.createChartData();
    this.initYieldChart();
  }

  ngOnDestroy() {
    this.resetChartInstance();
  }

  createChartData() {
    generalYieldChartArr = Object.values(this.yieldData.yieldChartData);

    console.log(generalYieldChartArr)

    this.yieldChartArr = generalYieldChartArr.map(el => (typeof el) === 'number' ? el : 0);

    this.yieldChartColorsArr = [];
    this.yieldChartLabels = [];
    this.yieldChartLabels.push(this.yieldData.code, 'Category');

    this.yieldChartArr.forEach((el, index) => {
      if (el > 0) {
        if (this.yieldChartLabels[index].toLowerCase() === 'category') {
          this.yieldChartColorsArr.push(this.chartColorBlue);
        } else {
          this.yieldChartColorsArr.push(this.chartColorGreen);
        }
      } else {
        this.yieldChartColorsArr.push(this.chartColorRed);
      }
    });

    this.profitabilityMinValue = Math.min(...this.yieldChartArr) < 0 ? Math.min(...this.yieldChartArr) : 0;
    this.profitabilityMaxValue = Math.max(...this.yieldChartArr) > 5 ? Math.max(...this.yieldChartArr) : 5;
    this.yAxesPadding = Math.min(...this.yieldChartArr) < 0 ? 60 : 0;
  }

  private initYieldChart() {
    this.resetChartInstance();

    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: this.yieldChartType,
      data: {
          labels: this.yieldChartLabels,
          datasets: [
            {
              data: this.yieldChartArr,
              backgroundColor: this.yieldChartColorsArr,
              hoverBackgroundColor: this.yieldChartColorsArr
            }
          ]
      },
      options: {
          responsive: true,
          maintainAspectRatio: false,
          tooltips: {
              enabled: false
          },
          legend: {
              display: false
          },
          layout: {
              padding: {
                  left: 0,
                  right: 70,
                  top: 0,
                  bottom: 0
              }
          },
          scales: {
              yAxes: [{
                  display: true,
                  scaleLabel: {
                    align: 'end'
                  },
                  barThickness: 30,
                  gridLines: {
                      color: 'transparent',
                      drawBorder: false
                  },
                  ticks: {
                      padding: this.yAxesPadding,
                      fontColor: '#101010',
                      fontStyle: 'bold',
                      fontSize: 12
                  }
              }],
              xAxes: [{
                  scaleLabel: {
                    align: 'end'
                  },
                  gridLines: {
                      display: true,
                      drawTicks: false,
                      drawBorder: true,
                      color: 'transparent',
                      zeroLineColor: '#C9C9C9'
                  },
                  ticks: {
                      padding: 0,
                      min: this.profitabilityMinValue,
                      max: this.profitabilityMaxValue,
                      callback: function(label: any, index: any, labels: any) {
                          return ' ';
                      }
                  }
              }]
          },
          hover: {
            animationDuration: 1
          },
          animation: {
            duration: 1,
            onComplete: function() {
              const chartInstance = this.chart;
              const ctx = chartInstance.ctx;
              let dataArr = [];

              ctx.textAlign = 'center';
              ctx.textBaseline = 'top';

              generalYieldChartArr.forEach(function(dataset: any, i: number) {
                typeof dataset === 'number' ? dataArr.push(dataset) : dataArr.push('N/A');
              });

              this.data.datasets.forEach(function(dataset: any, i: number) {
                let meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function(bar: any, index: any) {
                  let data = dataset.data[index];
                  if (data > 0) {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index].toFixed(2) + '%') : dataArr[index], bar._model.x + 30, bar._model.y - 5);
                  } else if (data < 0) {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index].toFixed(2) + '%') : dataArr[index], bar._model.x - 30, bar._model.y - 5);
                  } else {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index] + '%') : dataArr[index], bar._model.x + 30, bar._model.y - 5);
                  }
                });
              });
            }
          }
      }
    })
  }

  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }

}