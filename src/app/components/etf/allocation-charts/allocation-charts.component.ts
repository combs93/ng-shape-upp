import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-allocation-charts',
  templateUrl: './allocation-charts.component.html'
})
export class AllocationChartsComponent implements OnInit {

  @Input() doughnutData: any;

  private chartInstance: any;

  constructor() { }

  ngOnInit(): void {
    console.log(this.doughnutData)
  }

}