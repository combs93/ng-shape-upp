import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-bar-charts',
  templateUrl: './bar-charts.component.html'
})
export class BarChartsComponent implements OnInit {

  @Input() barsData: any;

  constructor() { }

  ngOnInit(): void {
    console.log(this.barsData)
  }

}
