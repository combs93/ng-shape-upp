import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-eft-benchmark',
  templateUrl: './eft-benchmark.component.html'
})
export class EftBenchmarkComponent implements OnInit {

  @Input() legend: any;

  constructor() { }

  ngOnInit(): void {
  }

}
