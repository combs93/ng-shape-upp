import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SuccessDialogComponent } from '../dialogs/success-dialog/success-dialog.component';

@Component({
  selector: 'app-subscription-form',
  templateUrl: './subscription-form.component.html'
})
export class SubscriptionFormComponent implements OnInit {

  subscribeForm: FormGroup;
  submitted = false;

  constructor(
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.subscribeForm = new FormGroup({
      email: new FormControl('', [Validators.email, Validators.required])
    });
  }

  submit() {
    this.submitted = true;
    console.log(this.subscribeForm.get('email'))
    if (!this.subscribeForm.get('email').invalid) {
      const dialogRef = this.dialog.open(SuccessDialogComponent);
  

      dialogRef.afterClosed().subscribe(result => {
        this.subscribeForm.reset();
      });
    }
  }

}
