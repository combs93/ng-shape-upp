import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-benchmark-index',
  templateUrl: './benchmark-index.component.html'
})
export class BenchmarkIndexComponent implements OnInit, OnChanges {

  @Input() benchmarkIndex: any;

  constructor() { }

  ngOnInit(): void {
  }
  
  ngOnChanges(): void {
  }

}
