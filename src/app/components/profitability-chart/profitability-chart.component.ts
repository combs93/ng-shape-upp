import { Component, OnInit, ViewChild, Input, ElementRef, OnDestroy, OnChanges } from '@angular/core';
declare var Chart: any;

let generalProfitabilityChartArr = [];

@Component({
  selector: 'app-profitability-chart',
  templateUrl: './profitability-chart.component.html'
})
export class ProfitabilityChartComponent implements OnInit, OnDestroy, OnChanges {
  @Input() profitabilityChart: any;
  @ViewChild('profitabilityChart', {static: true}) chartElemementRef: ElementRef;

  private chartInstance: any;

  chartColorRed = 'rgba(185,69,82,1)';
  chartColorGreen = 'rgba(16,167,95,1)';
  chartColorBlue = 'rgba(16,68,167,1)';

  profitabilityChartColorsArr = [];

  profitabilityChartType = 'horizontalBar';

  profitabilityChartArr = [];

  profitabilityMinValue: number;
  profitabilityMaxValue: number;
  yAxesPadding: number;

  profitabilityChartLabels = ['Operating Margin  ', 'Return Equity        '];

  profitabilityChartLegend = false;


  constructor() { }

  ngOnInit(): void {}

  ngOnChanges() {
    this.createChartData();
    this.initProfitabilityChart();
  }

  ngOnDestroy() {
    this.resetChartInstance();
  }

  createChartData() {
    generalProfitabilityChartArr = Object.values(this.profitabilityChart);

    this.profitabilityChartArr = generalProfitabilityChartArr.map(el => (typeof el) === 'number' ? el : 0);

    this.profitabilityChartColorsArr = [];

    this.profitabilityChartArr.forEach((el, index) => {
      if (el > 0) {
        if (this.profitabilityChartLabels[index].toLowerCase() === 'index') {
          this.profitabilityChartColorsArr.push(this.chartColorBlue);
        } else {
          this.profitabilityChartColorsArr.push(this.chartColorGreen);
        }
      } else {
        this.profitabilityChartColorsArr.push(this.chartColorRed);
      }
    });

    this.profitabilityMinValue = Math.min(...this.profitabilityChartArr) < 0 ? Math.min(...this.profitabilityChartArr) : 0;
    this.profitabilityMaxValue = Math.max(...this.profitabilityChartArr) > 100 ? Math.max(...this.profitabilityChartArr) : 100;
    // this.profitabilityMaxValue = 100;
    this.yAxesPadding = Math.min(...this.profitabilityChartArr) < 0 ? 60 : 0;
  }

  private initProfitabilityChart() {
    this.resetChartInstance();

    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: this.profitabilityChartType,
      data: {
          labels: this.profitabilityChartLabels,
          datasets: [
            {
              data: this.profitabilityChartArr,
              backgroundColor: this.profitabilityChartColorsArr,
              hoverBackgroundColor: this.profitabilityChartColorsArr
            }
          ]
      },
      options: {
          responsive: true,
          maintainAspectRatio: false,
          tooltips: {
              enabled: false
          },
          legend: {
              display: false
          },
          layout: {
              padding: {
                  left: 0,
                  right: 70,
                  top: 0,
                  bottom: 0
              }
          },
          scales: {
              yAxes: [{
                  display: true,
                  scaleLabel: {
                    position: 'left',
                    align: 'end'
                  },
                  barThickness: 30,
                  gridLines: {
                      color: 'transparent',
                      drawBorder: false
                  },
                  ticks: {
                      padding: this.yAxesPadding,
                      fontColor: '#101010',
                      fontStyle: 'bold',
                      fontSize: 12,
                      crossAlign: 'far'
                  }
              }],
              xAxes: [{
                scaleLabel: {
                  align: 'end'
                },
                gridLines: {
                  display: true,
                  drawTicks: false,
                  drawBorder: true,
                  color: 'transparent',
                  zeroLineColor: '#C9C9C9'
                },
                ticks: {
                  padding: 0,
                  min: this.profitabilityMinValue,
                  max: this.profitabilityMaxValue,
                  callback: function(label: any, index: any, labels: any) {
                      return ' ';
                  }
                }
              }]
          },
          hover: {
            animationDuration: 1
          },
          animation: {
            duration: 1,
            onComplete: function() {
              const chartInstance = this.chart;
              const ctx = chartInstance.ctx;
              let dataArr = [];

              ctx.textAlign = 'center';
              ctx.textBaseline = 'top';

              generalProfitabilityChartArr.forEach(function(dataset: any, i: number) {
                typeof dataset === 'number' ? dataArr.push(dataset) : dataArr.push('N/A');
              });

              this.data.datasets.forEach(function(dataset: any, i: number) {
                let meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function(bar: any, index: any) {
                  let data = dataset.data[index];
                  if (data > 0) {
                      ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index].toFixed(2) + '%') : dataArr[index], bar._model.x + 30, bar._model.y - 5);
                  } else if (data < 0) {
                      ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index].toFixed(2) + '%') : dataArr[index], bar._model.x - 30, bar._model.y - 5);
                  } else {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index] + '%') : dataArr[index], bar._model.x + 30, bar._model.y - 5);
                  }
                });
              });
            }
          }
      }
    })
  }

  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }

}
