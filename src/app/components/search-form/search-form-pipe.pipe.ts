import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFormPipe'
})
export class SearchFormPipePipe implements PipeTransform {

  transform(value: any, args: any): any {
    if (value) {
      // const filteredData = value.filter(el => el.code.toUpperCase().includes(args.toUpperCase()) || el.exchange.toUpperCase().includes(args.toUpperCase()));
      return value.length && args ? value : [{code: 'No matches founded', class: 'autocomplete-list__item__match'}];
    }
  }

}
