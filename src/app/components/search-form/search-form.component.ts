import { SearchContent } from './../../types/main.types';
import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MainService } from 'src/app/services/main.service';
import { debounceTime, distinctUntilChanged, switchMap, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html'
})
export class SearchFormComponent implements OnInit, OnDestroy {

  @Output() updateMainInfo = new EventEmitter<any>();

  searchForm: FormGroup;
  searchFormsubmitted = false;

  isLoading: 'SEARCH';
  searchContent = [];

  dropdownVisibilityState = false;

  private destroyed$: Subject<void> = new Subject();

  constructor(
    private mainService: MainService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.searchForm = new FormGroup({
      text: new FormControl('', [Validators.required])
    });
    this.subscribeForSearchValueChanges();
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  toggleDropdown() {
    this.dropdownVisibilityState = !this.dropdownVisibilityState;
    // this.dropdownVisibilityState = true;
  }

  private subscribeForSearchValueChanges(): void {
    this.searchForm.get('text').valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(val => {
          this.isLoading = 'SEARCH';
          // ! WARNING getSearchContent should be improved, need to provide dynamic value from search
          return this.mainService.getSearchContent(val);
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe((res: SearchContent) => {
        const resArr = [];
        res?.stocks?.forEach(item => {
          if (item.code.toLowerCase() === this.searchForm.value.text.toLowerCase()) {
            resArr.unshift(item)
          } else {
            resArr.push(item)
          }
        });
        this.searchContent = resArr;
        this.isLoading = null;
        console.log('11 - ', resArr);
      });
  }

  getUrlQueryParams(code: string, exchange: string, type: string) {
    if (type === 'ETF') {
      this.router.navigate(['/etfscreener/'], {
        queryParams: {
          code,
          exchange
        },
      });
    } else {
      this.router.navigate(['/screener/'], {
        queryParams: {
          code,
          exchange
        },
      });
    }
    console.log(this.searchForm);
    this.searchForm.patchValue({text: null});
    // this.updateMainInfo.emit({code, exchange});
  }
}
