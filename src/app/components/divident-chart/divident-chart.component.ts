import { Component, OnInit, ViewChild, Input, OnDestroy, ElementRef, OnChanges } from '@angular/core';
declare var Chart: any;

let generalDividendChartArr = [];

@Component({
  selector: 'app-divident-chart',
  templateUrl: './divident-chart.component.html'
})
export class DividentChartComponent implements OnInit, OnDestroy, OnChanges {

  @Input() dividendChart: any;
  @ViewChild('dividendChart', {static: true}) chartElemementRef: ElementRef;

  private chartInstance: any;

  chartColorRed = 'rgba(185,69,82,1)';
  chartColorGreen = 'rgba(16,167,95,1)';
  chartColorBlue = 'rgba(16,68,167,1)';

  dividendChartColorsArr = [];

  dividendChartType = 'horizontalBar';

  dividendChartArr = [];

  dividendMinValue: number;
  dividendMaxValue: number;
  yAxesPadding: number;

  dividendChartLabels = [];

  horizontalChartLegend = false;

  dividendChartOptions: any;

  dividendChartData: any;

  constructor() { }

  ngOnInit(): void {}

  ngOnChanges() {
    this.createChartData();
    this.initDividendChart();
  }

  ngOnDestroy() {
    this.resetChartInstance();
  }

  createChartData() {
    generalDividendChartArr = [
      this.dividendChart.chartData.stock_dividend_yield,
      typeof(this.dividendChart.chartData.index_dividend_yield) === 'number' ? this.dividendChart.chartData.index_dividend_yield / 100 : this.dividendChart.chartData.index_dividend_yield
    ];

    this.dividendChartArr = [typeof(this.dividendChart.chartData.stock_dividend_yield) === 'number' ? this.dividendChart.chartData.stock_dividend_yield : 0, typeof(this.dividendChart.chartData.index_dividend_yield) === 'number' ? this.dividendChart.chartData.index_dividend_yield / 100 : 0]
    this.dividendChartLabels = [this.dividendChart.paramsData.code ,'Index'];

    this.dividendChartColorsArr = [];
    this.dividendChartArr.forEach((el, index) => {
      if (el > 0) {
        if (this.dividendChartLabels[index].toLowerCase() === 'index') {
          this.dividendChartColorsArr.push(this.chartColorBlue);
        } else {
          this.dividendChartColorsArr.push(this.chartColorGreen);
        }
      } else {
        this.dividendChartColorsArr.push(this.chartColorRed);
      }
    });

    this.dividendMinValue = Math.min(...this.dividendChartArr) < 0 ? Math.min(...this.dividendChartArr) : 0;
    this.dividendMaxValue = Math.max(...this.dividendChartArr) > 5 ? Math.max(...this.dividendChartArr) : 5;
    this.yAxesPadding = Math.min(...this.dividendChartArr) < 0 ? 60 : 0;
  }


  private initDividendChart() {
    this.resetChartInstance();

    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: this.dividendChartType,
      data: {
          labels: this.dividendChartLabels,
          datasets: [
            {
              data: this.dividendChartArr,
              backgroundColor: this.dividendChartColorsArr,
              hoverBackgroundColor: this.dividendChartColorsArr
            }
          ]
      },
      options: {
          responsive: true,
          maintainAspectRatio: false,
          tooltips: {
              enabled: false
          },
          legend: {
              display: false
          },
          layout: {
              padding: {
                  left: 0,
                  right: 80,
                  top: 0,
                  bottom: 0
              }
          },
          scales: {
              yAxes: [{
                  display: true,
                  barThickness: 30,
                  gridLines: {
                      color: 'transparent',
                      drawBorder: false
                  },
                  ticks: {
                      padding: this.yAxesPadding,
                      fontColor: '#101010',
                      fontStyle: 'bold',
                      fontSize: 12
                  }
              }],
              xAxes: [{
                  gridLines: {
                      display: true,
                      drawTicks: false,
                      color: 'transparent',
                      zeroLineColor: '#C9C9C9'
                  },
                  ticks: {
                      padding: 0,
                      min: this.dividendMinValue,
                      max: this.dividendMaxValue,
                      callback: function(label: any, index: any, labels: any) {
                          return ' ';
                      }
                  }
              }]
          },
          hover: {
            animationDuration: 1
          },
          animation: {
            duration: 1,
            onComplete: function() {
              const chartInstance = this.chart;
              const ctx = chartInstance.ctx;
              let dataArr = [];

              ctx.textAlign = 'center';
              ctx.textBaseline = 'top';

              generalDividendChartArr.forEach(function(dataset: any, i: number) {
                typeof dataset === 'number' ? dataArr.push(dataset) : dataArr.push('N/A');
              });

              this.data.datasets.forEach(function(dataset: any, i: number) {
                const meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function(bar: any, index: any) {
                  const data = dataset.data[index];
                  if (data > 0) {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index].toFixed(2) + '%') : dataArr[index], bar._model.x + 30, bar._model.y - 5);
                  } else if (data < 0) {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index].toFixed(2) + '%') : dataArr[index], bar._model.x - 25, bar._model.y - 5);
                  } else {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index] + '%') : dataArr[index], bar._model.x + 30, bar._model.y - 5);
                  }
                });
              });
            }
          }
      }
    })
  }

  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }
}
