import { Component, OnInit } from '@angular/core';
import { MainChartService } from '../main-chart/main-chart.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-preloader',
  templateUrl: './preloader.component.html'
})
export class PreloaderComponent implements OnInit {

  loaderState: Observable<boolean>;

  constructor(
    private mainChartService: MainChartService
  ) { }

  ngOnInit(): void {
    this.loaderState = this.mainChartService.getLoader();
  }



}
