import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalystPriceComponent } from './analyst-price.component';

describe('AnalystPriceComponent', () => {
  let component: AnalystPriceComponent;
  let fixture: ComponentFixture<AnalystPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalystPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalystPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
