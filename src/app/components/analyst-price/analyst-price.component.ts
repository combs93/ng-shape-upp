import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-analyst-price',
  templateUrl: './analyst-price.component.html'
})
export class AnalystPriceComponent implements OnInit {

  @Input() analystPrice: any;
  currentAnalystValue: number;
  currentAnalystPosition: string;
  targetAnalystValue: number;
  targetAnalystPosition: string;

  constructor() { }

  ngOnInit(): void {
    this.setCurrentAnalystPosition();
    this.setTargetAnalystPosition();
  }

  setCurrentAnalystPosition() {
    this.currentAnalystValue = (this.analystPrice.current * 100 / this.analystPrice.week_high_52);
    return this.currentAnalystPosition = this.currentAnalystValue + '%';
  }
  
  setTargetAnalystPosition() {
    this.targetAnalystValue = (this.analystPrice.wall_street_target_price * 100 / this.analystPrice.week_high_52);
    return this.targetAnalystPosition = this.targetAnalystValue + '%';
  }

}
