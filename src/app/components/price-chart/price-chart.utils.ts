export const priceChartOptions = (indexArr: number[], chartArr: number[]) => {

  return {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
      enabled: true
    },
    hover: {mode: null},
    legend: {
        display: false
    },
    elements: {
        line: {
            tension: 0
        },
        point:{
            radius: 2
        }
    },
    scales: {
      yAxes: [{
        display: true,
        position: 'right',
        gridLines: {
            color: 'transparent',
            zeroLineColor: 'rgba(110, 110, 110, 1)',
            zeroLineBorderDash: [6, 4]
        },
        ticks: {
            beginAtZero: true,
            autoSkip: false,
            fontStyle: 'bold',
            stepSize: 0.01,
            callback(value: any, index: any, values: any) {
                if (chartArr && chartArr.length && value === chartArr[chartArr.length - 1]) {
                    return chartArr[chartArr.length - 1] > 0 ? '+' + value + '%' : ' ' + value + '%';
                } else if (indexArr && indexArr.length && value === indexArr[indexArr.length - 1]) {
                    return indexArr[indexArr.length - 1] > 0 ? '+' + value + '%' : ' ' + value + '%';
                } else if (value === 0) {
                    return value;
                } else {
                    return null;
                }
            }
        }
      }],
      xAxes: [{
        stacked: false,
        gridLines: {
            color: 'transparent',
            zeroLineColor: 'transparent'
        },
        ticks: {
            autoSkip: true,
            padding: 10,
            maxTicksLimit: 6,
            fontSize: '10',
            maxRotation: 0,
            callback(value: any, index: any, values: any) {
                return value;
            }
        }
      }]
    }
  };
};

export const priceChartData = (indexArr: number[], chartArr: number[], chartColorGreen, chartColorBlue) => {
  return [
    {
      data: indexArr,
      pointBackgroundColor: chartColorBlue,
      backgroundColor: chartColorBlue,
      borderColor: chartColorBlue,
      borderWidth: ['2'],
      borderDash: [4, 3],
      fill: false
    },
    {
      data: chartArr,
      pointBackgroundColor: chartColorGreen,
      backgroundColor: chartColorGreen,
      borderColor: chartColorGreen,
      borderWidth: ['2'],
      fill: false
    }
  ];
};
