import { MainChartService } from './../main-chart/main-chart.service';
import { Component, OnInit, Input, ElementRef, ViewChild, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { priceChartOptions, priceChartData } from './price-chart.utils';
declare var Chart: any;

@Component({
  selector: 'app-price-chart',
  templateUrl: './price-chart.component.html'
})
export class PriceChartComponent implements OnInit, OnDestroy, OnChanges {

  @Input() spyIsActive: boolean;
  @Input() queryParams: any;
  @Input() priceInfo: any;
  @ViewChild('priceChart', {static: true}) chartElemementRef: ElementRef;

  private chartInstance: any;

  chartColorRed = 'rgba(185,69,82,1)';
  chartColorGreen = 'rgba(16,167,95,1)';
  chartColorBlue = 'rgba(16,68,167,1)';

  chartArr: number[];
  chartArrClose: string[];
  indexArr: number[];
  indexArrClose: string[];

  priceChartOptions: any;
  priceChartData: any;
  priceChartLabels: string[];
  priceChartType = 'line';

  stockVectorState = true;
  indexVectorState = true;
  dataSets = [];


  constructor(
    private mainChartService: MainChartService
  ) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.resetChartInstance();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('spyIsActive')) {
      this.mainChartService.getPriceChart().subscribe(res => {
        this.createPriceChartData(res);
        this.initPriceChart(this.chartColorGreen, this.chartColorBlue);
        this.vectorStates(res);
      });
    }
  }

  createPriceChartData(chart: any) {
    console.log('chart: ', chart)

    let filteredStockChart = chart.stock.filter((el: any) => el.id >= 0);
    let filteredIndexChart: any;

    this.chartArr = filteredStockChart.map((el: any) => el.percentage).reverse();
    this.chartArrClose = filteredStockChart.map((el: any) => el.close).reverse();
    if (this.spyIsActive) {
      filteredIndexChart = chart.index && chart.index.length > 0 ? chart.index.filter((el: any) => el.id >= 0) : [];
      this.indexArr = filteredIndexChart.map((el: any) => el.percentage).reverse();
      this.indexArrClose = filteredIndexChart.map((el: any) => el.close).reverse();
      this.dataSets = [
        {
          data: this.indexArr,
          pointBackgroundColor: this.chartColorBlue,
          backgroundColor: this.chartColorBlue,
          borderColor: this.chartColorBlue,
          borderWidth: ['2'],
          borderDash: [4, 3],
          fill: false
        },
        {
          data: this.chartArr,
          pointBackgroundColor: this.chartColorGreen,
          backgroundColor: this.chartColorGreen,
          borderColor: this.chartColorGreen,
          borderWidth: ['2'],
          fill: false
        }
      ]
    } else {
      this.indexArr = [];
      this.indexArrClose = [];
      this.dataSets = [
        {
          data: this.chartArr,
          pointBackgroundColor: this.chartColorGreen,
          backgroundColor: this.chartColorGreen,
          borderColor: this.chartColorGreen,
          borderWidth: ['2'],
          fill: false
        }
      ]
    }

    this.priceChartLabels = filteredStockChart.map((el: any) => el.date).reverse();

    this.priceChartOptions = priceChartOptions(this.indexArr, this.chartArr);
    this.priceChartData = priceChartData(this.indexArr, this.chartArr, this.chartColorGreen, this.chartColorBlue);

  }

  private vectorStates(data: any) {
    if (!(data.index === null)) {
      const indexArr = data?.index.filter((el: any) => el.id >= 0);
      this.indexVectorState = ((+indexArr[0].close) - (+indexArr[indexArr.length - 1].close)) >= 0;
    }
    const stockArr = data?.stock.filter((el: any) => el.id >= 0);
    this.stockVectorState = ((+stockArr[0].close) - (+stockArr[stockArr.length - 1].close)) >= 0;

  }

  private initPriceChart(greenColor: string, blueColor: string) {
    this.resetChartInstance();

    Chart.defaults.priceChart = Chart.helpers.clone(Chart.defaults.line);
    Chart.controllers.priceChart = Chart.controllers.line.extend({
      update: function () {
        this.chart.data.datasets.forEach(element => {
          let min = Math.min.apply(null, element.data);
          let max = Math.max.apply(null, element.data);
          let yScale = this.getScaleForId(this.getMeta().yAxisID);
  
          let top = yScale.getPixelForValue(max);
          let zero = yScale.getPixelForValue(0);
          let bottom = yScale.getPixelForValue(min);
  
          let ctx = this.chart.chart.ctx;
          let ratio = Math.min((zero - top) / (bottom - top), 1);
  
          let gradient = ctx.createLinearGradient(0, top, 0, bottom);
          let line_gradient = ctx.createLinearGradient(0, top, 0, bottom);
          line_gradient.addColorStop(0, element.borderDash ? 'rgba(16,68,167,1)' : 'rgba(16,167,95,1)');
          line_gradient.addColorStop(ratio, element.borderDash ? 'rgba(16,68,167,1)' : 'rgba(16,167,95,1)');
          line_gradient.addColorStop(ratio, 'rgba(185,69,82,1)');
          line_gradient.addColorStop(1, 'rgba(185,69,82,1)');
  
          element.backgroundColor = gradient;
          element.borderColor = line_gradient;
          element.pointBackgroundColor = line_gradient;
  
          return Chart.controllers.line.prototype.update.apply(this, arguments)
        });
      }
    });


    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: 'priceChart',
      data: {
          labels: this.priceChartLabels,
          legend: {
              display: false
          },
          datasets: this.dataSets
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
          enabled: true
        },
        hover: {mode: null},
        legend: {
            display: false
        },
        elements: {
            line: {
                tension: 0
            },
            point:{
                radius: 2
            }
        },
        scales: {
          yAxes: [{
            display: true,
            position: 'right',
            gridLines: {
                color: 'transparent',
                zeroLineColor: 'rgba(110, 110, 110, 1)',
                zeroLineBorderDash: [6, 4]
            },
            ticks: {
                beginAtZero: true,
                autoSkip: false,
                fontStyle: 'bold',
                stepSize: 0.01,
                callback(value: any, index: any, values: any) {
                    if (this.chartArr && this.chartArr.length && value === this.chartArr[this.chartArr.length - 1]) {
                        return this.chartArr[this.chartArr.length - 1] > 0 ? '+' + value + '%' : ' ' + value + '%';
                    } else if (this.indexArr && this.indexArr.length && value === this.indexArr[this.indexArr.length - 1]) {
                        return this.indexArr[this.indexArr.length - 1] > 0 ? '+' + value + '%' : ' ' + value + '%';
                    } else if (value === 0) {
                        return value;
                    } else {
                        return null;
                    }
                }
            }
          }],
          xAxes: [{
            stacked: false,
            gridLines: {
                color: 'transparent',
                zeroLineColor: 'transparent'
            },
            ticks: {
                autoSkip: true,
                padding: 10,
                maxTicksLimit: 6,
                fontSize: '10',
                maxRotation: 0,
                callback(value: any, index: any, values: any) {
                    return value;
                }
            }
          }]
        }
      }
    });
  }
  
  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }

}
