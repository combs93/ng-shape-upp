import { Component, OnInit, HostListener, Inject, Input, Output, EventEmitter } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit {

  @Input() companyInfo: any;
  @Input() showStickyCompanyInfo: boolean;
  @Output() updateMainInfo = new EventEmitter<any>();

  menuState = false;
  visibleState = false;
  showPremiumBlock = true;

  mainPageInfo: any;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private mainService: MainService
  ) {}

  ngOnInit(): void {
    // premium block status
    // if (localStorage.getItem('showPremiumBlock')) {
    //   this.showPremiumBlock = !localStorage.getItem('showPremiumBlock');
    // }
  }


  // START menu States
  openMenu() {
    this.menuState = !this.menuState;
    this.document.body.classList.toggle('body-fixed');
  }

  closeMenu() {
    this.menuState = !this.menuState;
    this.document.body.classList.remove('body-fixed');
  }
  // END menu States


  // START premium block features
  // closePremiumBlock() {
  //   this.mainService.closePremiumBlock();
  //   this.showPremiumBlock = false;
  // }

  // subscribePremium() {
  //   this.mainService.subscribePremium();
  //   this.showPremiumBlock = false;
  // }
  // END premium block features



  // START fixed header state
  @HostListener('window:scroll')
  fixedHeaderListener() {
    const limitedHeight = 100;
    const scrollY = window.scrollY;
    this.visibleState = scrollY > limitedHeight;
  }
  // END fixed header state

}
