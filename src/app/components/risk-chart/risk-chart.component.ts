import { Component, OnInit, ViewChild, ElementRef, Input, OnDestroy, OnChanges } from '@angular/core';
declare var Chart: any;

let generalRiskChart = [];

@Component({
  selector: 'app-risk-chart',
  templateUrl: './risk-chart.component.html',
})
export class RiskChartComponent implements OnInit, OnDestroy, OnChanges {

  @Input() riskChart: any;
  @ViewChild('riskChart', {static: true}) chartElemementRef: ElementRef;

  private chartInstance: any;

  chartColorRed = 'rgba(185,69,82,1)';
  chartColorGreen = 'rgba(16,167,95,1)';
  chartColorBlue = 'rgba(16,68,167,1)';

  riskChartValues = [];
  riskChartIndex = [];
  commonValuesArr = [];

  riskChartLabels = ['Standard Deviation', 'Beta'];

  riskMinValue: number;
  riskMaxValue: number;
  yAxesPadding: number;

  riskChartType = 'bar';


  constructor() { }

  ngOnInit(): void {}

  ngOnChanges() {
    this.createChartData();
    this.initRiskChart();
  }

  ngOnDestroy() {
    this.resetChartInstance();
  }


  createChartData() {
    generalRiskChart = [Object.values(this.riskChart.stock), Object.values(this.riskChart.index)];

    this.riskChartValues = Object.values(this.riskChart.stock).map(el => (typeof el) === 'number' ? el : 0);
    this.riskChartIndex = Object.values(this.riskChart.index).map(el => (typeof el) === 'number' ? el : 0);

    this.commonValuesArr = this.riskChartValues.concat(this.riskChartIndex);

    this.riskMinValue = Math.min(...this.commonValuesArr) < 0 ? Math.min(...this.commonValuesArr) : 0;
    this.riskMaxValue = Math.max(...this.commonValuesArr) > 100 ? Math.max(...this.commonValuesArr) : 100;
    // this.riskMaxValue = 100;
    this.yAxesPadding = Math.min(...this.commonValuesArr) < 0 ? 50 : 0;
  }

  private initRiskChart() {
    this.resetChartInstance();

    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: this.riskChartType,
      data: {
        labels: this.riskChartLabels,
        borderSkipped: 'top',
        datasets: [
          {
            label: this.riskChart.code,
            backgroundColor: this.chartColorRed,
            hoverBackgroundColor: this.chartColorRed,
            data: this.riskChartValues
          },
          {
            label: 'Index',
            backgroundColor: this.chartColorBlue,
            hoverBackgroundColor: this.chartColorBlue,
            data: this.riskChartIndex
          }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
          enabled: false
        },
        legend: {
          display: false
        },
        layout: {
            padding: {
              left: 0,
              right: 30,
              top: 30,
              bottom: 0
            }
        },
        scales: {
            yAxes: [{
                display: true,
                gridLines: {
                  color: 'transparent',
                  zeroLineColor: '#C9C9C9',
                  drawTicks: false,
                  drawBorder: false
                },
                ticks: {
                  padding: this.yAxesPadding,
                  fontStyle: 'bold',
                  min: this.riskMinValue,
                  max: this.riskMaxValue,
                  callback(value: any, index: any, labels: any) {
                      return ' ';
                  }
                }
            }],
            xAxes: [{
                barThickness: 30,
                gridLines: {
                  display: false,
                  drawTicks: false,
                  color: 'transparent',
                  drawBorder: false
                },
                ticks: {
                  padding: 50,
                  fontColor: '#6F6E6E',
                  fontSize: 11,
                  callback(label: any, index: any, labels: any) {
                    if (/\s/.test(label)) {
                      return label.split(' ');
                    } else{
                      return label;
                    }
                  }
                }
            }]
        },
        hover: {
          animationDuration: 1
        },
        animation: {
          duration: 1,
          onComplete() {
            const chartInstance = this.chart;
            const ctx = chartInstance.ctx;

            ctx.textAlign = 'center';
            ctx.textBaseline = 'top';

            this.data.datasets.forEach(function(dataset: any, i: number): void {
              const meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function(bar: any, index: any) {
                const data = dataset.data[index];
                const postfix = index === 0 ? '%' : '';

                if (data > 0) {
                  ctx.fillText(typeof generalRiskChart[i][index] === 'number' ? (generalRiskChart[i][index].toFixed(2) + postfix) : 'N/A', bar._model.x, bar._model.y - 15);
                  ctx.fillText(dataset.label, bar._model.x, 250);
                } else if (data < 0) {
                  ctx.fillText(typeof generalRiskChart[i][index] === 'number' ? (generalRiskChart[i][index].toFixed(2) + postfix) : 'N/A', bar._model.x, bar._model.y + 10);
                  ctx.fillText(dataset.label, bar._model.x, 250);
                } else {
                  ctx.fillText(typeof generalRiskChart[i][index] === 'number' ? (generalRiskChart[i][index] + postfix) : 'N/A', bar._model.x, bar._model.y - 15);
                  ctx.fillText(dataset.label, bar._model.x, 250);
                }
              });
            });
          }
        }
      }
    })
  }

  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }

}
