import { Component, OnInit, ViewChild, Input, ElementRef, OnDestroy, OnChanges } from '@angular/core';
declare var Chart: any;

let generalGrowthChartArr = [];

@Component({
  selector: 'app-growth-chart',
  templateUrl: './growth-chart.component.html'
})
export class GrowthChartComponent implements OnInit, OnDestroy, OnChanges {

  @Input() growthChart: any;
  @ViewChild('growthChart', {static: true}) chartElemementRef: ElementRef;

  private chartInstance: any;

  chartColorRed = 'rgba(185,69,82,1)';
  chartColorGreen = 'rgba(16,167,95,1)';
  chartColorBlue = 'rgba(16,68,167,1)';

  growthChartColorsArr = [];

  growthChartType = 'horizontalBar';

  growthChartArr = [];

  growthMinValue: number;
  growthMaxValue: number;
  yAxesPadding: number;

  growthChartLabels = ['Revenue', 'Earnings'];

  growthChartLegend = false;

  growthChartData: any;

  constructor() { }

  ngOnInit(): void {}

  ngOnChanges() {
    this.createChartData();
    this.initGrowthChart();
  }

  ngOnDestroy() {
    this.resetChartInstance();
  }

  createChartData() {
    generalGrowthChartArr = Object.values(this.growthChart);

    this.growthChartArr = generalGrowthChartArr.map(el => (typeof el) === 'number' ? el : 0);

    this.growthChartColorsArr = [];

    this.growthChartArr.forEach((el, index) => {
      if (el > 0) {
        if (this.growthChartLabels[index].toLowerCase() === 'index') {
          this.growthChartColorsArr.push(this.chartColorBlue);
        } else {
          this.growthChartColorsArr.push(this.chartColorGreen);
        }
      } else {
        this.growthChartColorsArr.push(this.chartColorRed);
      }
    });

    this.growthMinValue = Math.min(...this.growthChartArr) < 0 ? Math.min(...this.growthChartArr) : 0;
    this.growthMaxValue = Math.max(...this.growthChartArr) > 100 ? Math.max(...this.growthChartArr) : 100;
    // this.growthMaxValue = 100;
    this.yAxesPadding = Math.min(...this.growthChartArr) < 0 ? 60 : 0;
  }

  private initGrowthChart() {
    this.resetChartInstance();

    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: this.growthChartType,
      data: {
          labels: this.growthChartLabels,
          datasets: [
            {
              data: this.growthChartArr,
              backgroundColor: this.growthChartColorsArr,
              hoverBackgroundColor: this.growthChartColorsArr
            }
          ]
      },
      options: {
          responsive: true,
          maintainAspectRatio: false,
          tooltips: {
              enabled: false
          },
          legend: {
              display: false
          },
          layout: {
              padding: {
                  left: 0,
                  right: 70,
                  top: 0,
                  bottom: 0
              }
          },
          scales: {
              yAxes: [{
                  display: true,
                  barThickness: 30,
                  gridLines: {
                      color: 'transparent',
                      drawBorder: false
                  },
                  ticks: {
                      padding: this.yAxesPadding,
                      fontColor: '#101010',
                      fontStyle: 'bold',
                      fontSize: 12
                  }
              }],
              xAxes: [{
                  gridLines: {
                      display: true,
                      drawTicks: false,
                      drawBorder: true,
                      color: 'transparent',
                      zeroLineColor: '#C9C9C9'
                  },
                  ticks: {
                      padding: 0,
                      min: this.growthMinValue,
                      max: this.growthMaxValue,
                      callback: function(label: any, index: any, labels: any) {
                          return ' ';
                      }
                  }
              }]
          },
          hover: {
            animationDuration: 1
          },
          animation: {
            duration: 1,
            onComplete: function() {
              const chartInstance = this.chart;
              const ctx = chartInstance.ctx;
              let dataArr = [];

              ctx.textAlign = 'center';
              ctx.textBaseline = 'top';

              generalGrowthChartArr.forEach(function(dataset: any, i: number) {
                typeof dataset === 'number' ? dataArr.push(dataset) : dataArr.push('N/A');
              });

              this.data.datasets.forEach(function(dataset: any, i: number) {
                let meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function(bar: any, index: any) {
                  let data = dataset.data[index];
                  if (data > 0) {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index].toFixed(2) + '%') : dataArr[index], bar._model.x + 30, bar._model.y - 5);
                  } else if (data < 0) {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index].toFixed(2) + '%') : dataArr[index], bar._model.x - 30, bar._model.y - 5);
                  } else {
                    ctx.fillText(typeof dataArr[index] === 'number' ? (dataArr[index] + '%') : dataArr[index], bar._model.x + 30, bar._model.y - 5);
                  }
                });
              });
            }
          }
      }
    })
  }

  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }

}
