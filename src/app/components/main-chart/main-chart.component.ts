import { config } from './../../app.config';
import { ActivatedRoute, Params } from '@angular/router';
import { MainChartService } from './main-chart.service';
import { Observable } from 'rxjs';
import { pluck, tap, filter } from 'rxjs/operators';
import { MainService } from 'src/app/services/main.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-main-chart',
  templateUrl: './main-chart.component.html'
})
export class MainChartComponent implements OnInit {

  @Input() defaultChartPeriod: string;
  @Input() benchmarkIndex: any;
  @Input() spyIsActive: boolean;

  periods = [
    {name: '1M', isActive: false, disabled: false},
    {name: '3M', isActive: false, disabled: false},
    {name: 'YTD', isActive: true, disabled: false},
    {name: '1Y', isActive: false, disabled: false},
    {name: '3Y', isActive: false, disabled: false},
    {name: '5Y', isActive: false, disabled: false},
    {name: '10Y', isActive: false, disabled: false},
    {name: 'MAX', isActive: false, disabled: false}
  ];

  setDefaultPeriod = false;
  queryParams: Params = config.defaultParams;
  epsChartState$: Observable<any>;

  // spyIsActive = true;

  constructor(
    private mainService: MainService,
    private mainChartService: MainChartService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.pipe(filter(el => el && el.code)).subscribe(res => {
      this.queryParams = this.mainService.getQueryParamsForMainPage(res);
    });

    this.mainChartService.getPriceChart().subscribe(res => {
      if (this.setDefaultPeriod) {
        this.listClick(false, 2, {name: 'YTD'});
      }
      this.setDefaultPeriod = true;
    });
  }

  listClick(update: boolean = false, selectedIndex: number, period: any) {
    this.setDefaultPeriod = false;
    this.periods.map((el, i) => {
      el.isActive = i === selectedIndex;
      return el;
    });
    if (update) {
      this.updatePriceChart(period.name);
    }
  }

  updatePriceChart(period: string) {
    this.mainChartService.setLoader(true);
    this.mainService.getMainContent(this.queryParams, period)
    .pipe(pluck('chart'), tap(() => this.mainChartService.setLoader(false)))
    .subscribe(res => {
        this.mainChartService.setPriceChart(res.price);
        this.mainChartService.setEpsChart(res.earnings_per_share);
        // this.mainChartService.setVectors(res.vector);
      });
  }
}
