import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MainChartService {

  private priceChart = new BehaviorSubject<any>({});
  private epsChart = new BehaviorSubject<any>({});
  // private vectorComparison = new BehaviorSubject<any>(true);
  private loader = new BehaviorSubject<boolean>(false);

  constructor() { }

  getPriceChart(): Observable<any> {
    return this.priceChart.asObservable();
  }

  setPriceChart(data: any) {
    console.log('data: ', data)
    this.priceChart.next(data);
  }

  getEpsChart(): Observable<any> {
    return this.epsChart.asObservable();
  }

  setEpsChart(data: any) {
    this.epsChart.next(data);
  }

  // getVectors(): boolean {
  //   return this.vectorComparison.getValue();
  // }

  // setVectors(data: any) {
  //   if (data.stock[0].close && +data.stock[1].close) {
  //     const stockVectorDirection = +data.stock[0].close > +data.stock[1].close;
  //     this.vectorComparison.next(stockVectorDirection);
  //   }
  // }

  getLoader(): Observable<boolean> {
    return this.loader.asObservable();
  }

  setLoader(data: boolean) {
    this.loader.next(data);
  }
}
