declare var Chart: any;

export const earningsChartCustomType: any = (earningsChartLabels: number[]) => {
    // Chart.defaults.EarningsChart = Chart.helpers.clone(Chart.defaults.line);

    // Chart.controllers.EarningsChart = Chart.controllers.line.extend({
    //   update() {
    // const min = 5;
    // const max = 10;
    // const yScale = this.getScaleForId(this.getMeta().yAxisID);
    // console.log('min - ', min);
    // console.log('max - ', max);

    // const top = yScale.getPixelForValue(max);
    // const zero = yScale.getPixelForValue(0);
    // const bottom = yScale.getPixelForValue(min);

    // const ctx = this.chart.chart.ctx;
    // const ratio = Math.min((zero - top) / (bottom - top), 1);

    // const lineGradient = ctx.createLinearGradient(0, top, 0, bottom);
    // lineGradient.addColorStop(0, 'rgba(16,167,95,1)');
    // lineGradient.addColorStop(ratio, 'rgba(16,167,95,1)');
    // lineGradient.addColorStop(ratio, 'rgba(185,69,82,1)');
    // lineGradient.addColorStop(1, 'rgba(185,69,82,1)');

    // this.chart.data.datasets[0].pointBackgroundColor = lineGradient;
    // this.chart.data.datasets[0].pointHoverBackgroundColor	= lineGradient;

    //     return Chart.controllers.line.prototype.update.apply(this, arguments);
    //   }
    // });
};


export const earningsChartOptions: any = (ChartxAxesColorsArr: string[]) => {
  return {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
        display: false
    },
    title: {
        display: true
    },
    tooltips: {
        enabled: false
    },
    elements: {
        line: {
            tension: 0
        }
    },
    scales: {
      yAxes: [
          {
              gridLines: {
                  display: false,
                  drawBorder: false
              },
              ticks: {
                  fontSize: 12,
                  beginAtZero: false,
                  fontColor: 'rgba(#3E3E3E, 0.5)',
                  callback(value: any, index: any, values: any) {
                      if (value === 0) {
                          return '';
                      } else {
                          return null;
                      }
                  }
              }
          }
      ],
      xAxes: [
        {
          gridLines: {
              display: false,
              drawBorder: true,
              color: '#ECECEC'
          },
          stacked: true,
          ticks: {
              max: 50,
              beginAtZero: false,
              min: 0,
              fontSize: 12,
              fontColor: ChartxAxesColorsArr,
              stepSize: 0.1,
              callback(value: any, index: any, values: any) {
                return value + '%';
              }
          }
        }
      ]
    }
  };
};

export const earningsChartData = (chartColorBlue: string, chartxAxesColorsArr: string[], earningsChartIndex: number[], earningsChartLabels: number[]) => {
  return [
    {
      data: earningsChartLabels,
      pointBorderWidth: '0',
      pointBorderColor: 'transparent',
      pointRadius: '5',
      pointHoverBorderWidth: '0',
      pointBackgroundColor: chartxAxesColorsArr,
      borderColor: 'transparent',
      backgroundColor: 'transparent',
      pointHoverRadius: '5',
      borderWidth: '2',
      borderJoinStyle: 'bevel'
    },
    {
      data: earningsChartIndex,
      pointBorderWidth: '0',
      pointBorderColor: 'transparent',
      pointRadius: '5',
      borderColor: 'transparent',
      backgroundColor: 'transparent',
      pointHoverBorderWidth: '0',
      pointBackgroundColor: chartColorBlue,
      pointHoverRadius: '5',
      borderWidth: '2',
      borderJoinStyle: 'bevel'
    }
  ];
};
