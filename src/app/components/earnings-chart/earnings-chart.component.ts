import { MainChartService } from './../main-chart/main-chart.service';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy, OnChanges, Output, EventEmitter } from '@angular/core';
declare var Chart: any;

import { earningsChartOptions, earningsChartData, earningsChartCustomType } from './earnings-chart.utils';


@Component({
  selector: 'app-earnings-chart',
  templateUrl: './earnings-chart.component.html'
})

export class EarningsChartComponent implements OnInit, OnDestroy, OnChanges {

  @ViewChild('earningsChart', {static: true}) chartElemementRef: ElementRef;

  private chartInstance: any;

  chartColorRed = 'rgba(185,69,82,1)';
  chartColorGreen = 'rgba(16,167,95,1)';
  chartColorBlue = 'rgba(16,68,167,1)';
  chartColorDark = 'rgba(0,0,0,1)';

  ChartxAxesColorsArr = [];
  earningsChartOptions = {};

  epsActual: any[];
  epsEstimate: any[];
  epsSurprisePercent: any[];

  showEpsChart = true;

  earningsChartType = 'EarningsChart';
  earningsChartData: any;


  constructor(private mainChartService: MainChartService) {}

  ngOnInit(): void {
    this.mainChartService.getEpsChart().subscribe(res => {
      this.createEpsChartData(res);
      // earningsChartCustomType(this.epsConcatedArr);
      // this.createChartxAxesColorsArr();
      this.initEarningsChart();
      this.showEpsChart = this.epsChartVisibility();
    });
  }

  ngOnDestroy() {
    this.resetChartInstance();
  }

  ngOnChanges() {
  }

  createEpsChartData(epsChart: any) {
    this.epsActual = Object.values(epsChart)
      .map((el: any) => (el.epsEstimate === null) || (el.epsActual === null) ? null : (+el.epsActual).toFixed(1))
      .filter(el => (el !== null));

    this.epsEstimate = Object.values(epsChart)
      .map((el: any) => (el.epsActual === null) || (el.epsEstimate === null) ? null : (+el.epsEstimate).toFixed(1))
    .filter(el => (el !== null));

    // console.log('this.epsActual - ', this.epsActual);
    // console.log('this.epsEstimate - ', this.epsEstimate);

    this.epsSurprisePercent = Object.values(epsChart)
      // tslint:disable-next-line: max-line-length
      .map((el: any) => (el.epsEstimate === null) || (el.epsActual === null) || (el.surprisePercent === null) ? null : (+el.surprisePercent).toFixed(1))
      .filter(el => (el !== null));

    // console.log('this.epsSurprisePercent - ', this.epsSurprisePercent);

    // this.epsConcatedArr = this.epsActual.concat(this.epsEstimate);
    // console.log(this.epsConcatedArr);
    this.ChartxAxesColorsArr = [];

    this.epsSurprisePercent.forEach((el) => {
      if (el > 0) {
        this.ChartxAxesColorsArr.push(this.chartColorGreen);
      } else if (el < 0) {
        this.ChartxAxesColorsArr.push(this.chartColorRed);
      }
    });

    this.earningsChartData = earningsChartData(this.chartColorBlue, this.ChartxAxesColorsArr, this.epsEstimate, this.epsActual);
    this.earningsChartOptions = earningsChartOptions(this.ChartxAxesColorsArr);
  }

  epsChartVisibility(): boolean {
    return this.epsActual.length === 0 || this.epsEstimate.length === 0 || this.epsSurprisePercent.length === 0;
  }

  // createChartxAxesColorsArr() {}

  private initEarningsChart() {
    this.resetChartInstance();

    this.chartInstance = new Chart(this.chartElemementRef.nativeElement, {
      type: 'line',
      options: this.earningsChartOptions,
      data: {
        labels: this.epsSurprisePercent,
        datasets: this.earningsChartData
      }
    });
  }

  private resetChartInstance(): void {
    if (this.chartInstance) {
      this.chartInstance.destroy();
    }
  }

}
